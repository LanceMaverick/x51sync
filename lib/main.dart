import 'package:colours/colours.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
//import 'package:flutter_config/flutter_config.dart';
import 'package:f_logs/f_logs.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:window_manager/window_manager.dart';
import 'package:x51sync/auth/auth_repository.dart';
import 'package:x51sync/auth/login/login_view.dart';
import 'package:x51sync/src/livery_upload/bloc/liveryupload_bloc.dart';
import 'package:x51sync/src/livery_upload/bloc/liveryupload_event.dart';
import 'package:x51sync/src/services/api_service.dart';
import 'package:x51sync/src/services/file_service.dart';
import 'package:x51sync/src/services/sync_service.dart';
import 'package:x51sync/src/session/bloc/session_bloc.dart';
import 'package:x51sync/tools/window.dart';
import 'package:x51sync/views/home/home.dart';
import 'package:x51sync/views/manage/access_denied.dart';
import 'package:x51sync/views/manage/admin.dart';
import 'package:x51sync/views/manage/edit_mod.dart';
import 'package:x51sync/views/manage/liveries.dart';
import 'package:x51sync/views/manage/manage_livery.dart';
import 'package:x51sync/views/manage/mods.dart';
import 'package:x51sync/views/manage/new_livery.dart';
import 'package:x51sync/views/manage/new_livery_aircraft.dart';
import 'package:x51sync/views/manage/new_mod.dart';
import 'package:x51sync/src/file_sync/bloc/files_bloc.dart';
import 'package:x51sync/src/file_sync/bloc/files_event.dart';
import 'package:x51sync/views/manage/upload_complete.dart';
import 'package:flutter_window_close/flutter_window_close.dart';
import 'package:x51sync/src/services/globals.dart';
import 'package:shared_preferences/shared_preferences.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  LogsConfig config = FLog.getDefaultConfigurations()
    ..isDevelopmentDebuggingEnabled = true
    ..timestampFormat = TimestampFormat.TIME_FORMAT_FULL_3
    ..formatType = FormatType.FORMAT_CUSTOM
    ..fieldOrderFormatCustom = [
      FieldName.TIMESTAMP,
      FieldName.LOG_LEVEL,
      FieldName.CLASSNAME,
      FieldName.METHOD_NAME,
      FieldName.TEXT,
      FieldName.EXCEPTION,
      FieldName.STACKTRACE
    ]
    ..customOpeningDivider = "{"
    ..customClosingDivider = "}";

  FLog.applyConfigurations(config);
  FlutterError.onError = (FlutterErrorDetails details) {
    FLog.severe(
        text: "UNHANDLED EXCEPTION: ${details.exception}",
        stacktrace: details.stack);
  };
  WidgetsFlutterBinding.ensureInitialized();
  // await FlutterConfig.loadEnvVariables();
  await windowManager.ensureInitialized();
  resizeWindow();
  await FLog.clearLogs();
  DateTime now = DateTime.now();
  final dtStr = DateFormat('yyyy-MM-dd–kk-mm-ss').format(now);
  FLog.info(text: "NEW LOG. APP STARTED:$dtStr");
  //runZonedGuarded(() async {

  FlutterWindowClose.setWindowShouldCloseHandler(() async {
    Size size = await windowManager.getSize();
    SharedPreferences prefs = await SharedPreferences.getInstance();
    await prefs.setInt('windowHeight', size.height.toInt());
    await prefs.setInt('windowWidth', size.width.toInt());
    return true;
  });

  runApp(
    RepositoryProvider(
      create: (_) => ApiService(),
      lazy: false,
      child: MultiBlocProvider(
        providers: [
          BlocProvider(
            create: (context) => FileSyncBloc(
                syncSvc: SyncService(),
                apiSvc: RepositoryProvider.of<ApiService>(context),
                fileSvc: FileService())
              ..add(FileSyncInit()),
          ),
          BlocProvider(
            lazy: false,
            create: (context) => LiveryUploadBloc(
                syncSvc: SyncService(),
                apiSvc: RepositoryProvider.of<ApiService>(context),
                fileSvc: FileService())
              ..add(LiveryUploadInit()),
          ),
          BlocProvider(
            lazy: false,
            create: (context) =>
                SessionBloc(apiSvc: RepositoryProvider.of<ApiService>(context))
                  ..add(SessionInit()),
          ),
        ],
        child: const MyApp(),
      ),
    ),
  );
  // }, (error, trace) {
  //   FLog.severe(text: "UNHANDLED EXCEPTION: $error", stacktrace: trace);
  // }
//  );
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      //home: const Home(),
      home: RepositoryProvider(
          create: (context) => AuthRepository(), child: LoginView()),
      routes: {
        '/home': (context) => const HomeView(),
        '/login': (context) => LoginView(),
        '/mods': (context) => const ModsView(),
        '/edit_mod': (context) => EditModView(),
        '/new_mod': (context) => NewModView(),
        '/upload_complete': (context) => const UploadCompleteView(),
        '/liveries': (context) => const LiveriesView(),
        '/new_livery_aircraft': (context) => NewAircraftView(),
        '/new_livery': (context) => const NewLiveryView(),
        '/edit_livery': (context) => const ManageLiveryView(),
        '/access_denied': (context) => const AccessDeniedView(),
        '/admin': (context) => const AdminView(),
      },
      theme: ThemeData(
        textTheme: GoogleFonts.russoOneTextTheme(),
        colorScheme: ColorScheme.fromSwatch(primarySwatch: Colours.fireBrick),
      ),
    );
  }
}
