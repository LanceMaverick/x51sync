import 'dart:async';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:window_manager/window_manager.dart';
import 'package:x51sync/src/services/globals.dart';

Future resizeWindow({reset = false}) async {
  SharedPreferences prefs = await SharedPreferences.getInstance();
  num height = prefs.getInt('windowHeight') ?? GlobalData.defaultHeight;
  num width = prefs.getInt('windowWidth') ?? GlobalData.defaultWidth;

  if (reset) {
    await prefs.setInt('windowHeight', GlobalData.defaultHeight.toInt());
    await prefs.setInt('windowWidth', GlobalData.defaultWidth.toInt());

    height = GlobalData.defaultHeight;
    width = GlobalData.defaultWidth;
  }

  WindowOptions windowOptions = WindowOptions(
    size: Size(width.toDouble(), height.toDouble()),
    center: true,
    // backgroundColor: Colors.transparent,
    //skipTaskbar: false,
    titleBarStyle: TitleBarStyle.normal,
  );
  windowManager.waitUntilReadyToShow(windowOptions, () async {
    await windowManager.show();
    await windowManager.focus();
  });
}
