import 'package:flutter/material.dart';
import 'package:x51sync/views/widgets/log_exporter.dart';
import 'package:x51sync/views/widgets/settings.dart';

class HomeView extends StatelessWidget {
  const HomeView({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    //context.read<FileSyncBloc>().add(FileSyncInit());
    Column _buildButtonColumn(
        Color color1, Color color2, IconData icon, String label, String route) {
      return Column(
        mainAxisSize: MainAxisSize.min,
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          ElevatedButton(
              onPressed: () {
                Navigator.pushNamed(context, route);
              },
              child: Icon(icon, color: color1, size: 100)),
          Container(
            margin: const EdgeInsets.only(top: 8),
            child: Text(
              label,
              style: TextStyle(
                fontSize: 12,
                fontWeight: FontWeight.w400,
                color: color2,
              ),
            ),
          ),
        ],
      );
    }

    Widget logo = Container(
        padding: const EdgeInsets.all(40),
        child: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            mainAxisSize: MainAxisSize.max,
            crossAxisAlignment: CrossAxisAlignment.end,
            children: const [
              Image(
                image: AssetImage('assets/images/x51_150.png'),
              )
            ]));
    Color color1 = Colors.grey.shade200;
    Color color2 = Colors.grey.shade900;

    Widget buttonSection = Container(
      padding: const EdgeInsets.fromLTRB(50, 40, 50, 40),
      child: Row(mainAxisAlignment: MainAxisAlignment.spaceEvenly, children: [
        _buildButtonColumn(
            color1, color2, Icons.airplanemode_active, 'Mods', '/mods'),
        _buildButtonColumn(color1, color2, Icons.format_paint_rounded,
            'Liveries', '/liveries'),
      ]),
    );

    return Scaffold(
      appBar: AppBar(
          title: const Text('X51 ModSync'), actions: [exportButton(context)]),
      body: Column(
        children: [
          logo,
          Center(child: buttonSection),
          const Spacer(),
          Align(
              alignment: Alignment.topCenter,
              child: Padding(
                padding: const EdgeInsets.all(8.0),
                child: Settings(),
              ))
        ],
      ),
    );
  }
/*
    
    Widget buttonSection = Container(
      padding: const EdgeInsets.all(16.0),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: [
              _buildButtonColumn(color, Icons.upload, 'Upload New Mod'),
              _buildButtonColumn(color, Icons.update, 'Update Mod'),
              _buildButtonColumn(color, Icons.sync, 'Sync Mods'),
            ],
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: [
              _buildButtonColumn(color, Icons.upload, 'Upload New Livery'),
              _buildButtonColumn(color, Icons.update, 'Update Mod'),
              _buildButtonColumn(color, Icons.sync, 'Sync Mods'),
            ],
          )
        ],
      ),
    );

    return Scaffold(
      appBar: AppBar(
        title: const Text('X51 ModSync'),
      ),
      body: Stack(
        children: [
          //Align(
          //  child: _logo,
          //  alignment: Alignment.topCenter,
          //),
          Center(child: buttonSection),
          Align(
              alignment: Alignment.bottomCenter,
              child: Padding(
                padding: const EdgeInsets.all(8.0),
                child: Settings(),
              ))
        ],
      ),
    );
  }

  */
}
