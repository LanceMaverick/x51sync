import 'dart:io';

import 'package:expandable_group/expandable_group.dart';
import 'package:expandable_group/expandable_group_widget.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:x51sync/models/files.dart';
import 'package:x51sync/models/ui.dart';
import 'package:x51sync/src/file_sync/bloc/files_bloc.dart';
import 'package:x51sync/src/file_sync/bloc/files_event.dart';
import 'package:x51sync/src/file_sync/bloc/files_state.dart';
import 'package:x51sync/src/file_sync/data_state.dart';
import 'package:x51sync/src/livery_upload/bloc/liveryupload_bloc.dart';
import 'package:x51sync/src/livery_upload/bloc/liveryupload_event.dart';
import 'package:x51sync/src/services/api_service.dart';
import 'package:x51sync/src/services/file_service.dart';
import 'package:x51sync/src/services/sync_service.dart';
import 'package:x51sync/src/services/x51colors.dart';
import 'package:x51sync/src/session/bloc/session_bloc.dart';
import 'package:x51sync/views/manage/new_livery.dart';
import 'package:x51sync/views/widgets/api_error_handler.dart';
import 'package:x51sync/views/widgets/log_exporter.dart';
import 'package:x51sync/views/widgets/sync_panel.dart';

class LiveriesView extends StatelessWidget {
  const LiveriesView({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocListener<SessionBloc, SessionState>(
      listener: (context, state) {
        checkApiErrors(context, state);
      },
      child: BlocBuilder<FileSyncBloc, FileSyncState>(
        builder: (context, state) {
          return Scaffold(
            appBar: AppBar(
                title: const Text('Liveries'),
                actions: [exportButton(context)]),
            body: BlocProvider(
              create: (context) => LiveryUploadBloc(
                  fileSvc: FileService(),
                  syncSvc: SyncService(),
                  apiSvc: context.read<ApiService>())
                ..add(LiveryUploadInit()),
              child: Container(
                padding:
                    const EdgeInsets.symmetric(vertical: 0, horizontal: 16),
                child: Stack(children: const [LiveryPanel()]),
              ),
            ),
          );
        },
      ),
    );
  }
}

class LiveryPanel extends StatelessWidget {
  const LiveryPanel({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<FileSyncBloc, FileSyncState>(builder: (context, state) {
      List<Widget> children = [];
      final dataStatus = state.livDataStatus;
      if (dataStatus is DataRefreshing) {
        String msg = "Refreshing data...";
        if (dataStatus is RemoteDataRefreshing) {
          msg = "Checking remote files...";
        } else if (dataStatus is SyncDataRefreshing) {
          msg = "Generating deltas...";
        }
        SpinKitCubeGrid spinkit = SpinKitCubeGrid(
          color: Theme.of(context).colorScheme.primary,
          size: 200.0,
        );
        children.add(Container(
          padding: const EdgeInsets.all(32.0),
          alignment: Alignment.center,
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: [
              Padding(
                  padding: const EdgeInsets.all(200),
                  child: Column(
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      children: [spinkit, Text(msg)])),
            ],
          ),
        ));
      } else if (dataStatus is InitialDataStatus) {
        children.add(Container(
            alignment: Alignment.center,
            child: const Text('Click refresh to see sync status')));
      } else if (dataStatus is DataRefreshFailed) {
        Object error = dataStatus.exception;
        String err = '';
        //TODO - move to bloc
        if (error is FileSystemException) {
          err =
              'Make sure you DCS saved games folder is set correctly, and all mod folders are present';
        } else {
          err = error.toString();
        }
        //TODO: use exception type
        if (err.contains('401')) {
          context.read<FileSyncBloc>().add(FileSyncInit());
          return Container(); //TODO
          //showExpired(context);
        }
        children.add(Container(
            alignment: Alignment.center,
            child: Text('Error retrieving data: $err')));
      } else if (dataStatus is DataRefreshed) {
        children.add(const Expanded(
            child: Card(
                elevation: 5,
                child: SizedBox(height: 500, child: LiveryList()))));
        //children.add(const Text('done!'));
      } else if (dataStatus is DataSyncing) {
        Widget progbar = syncPanel(state.syncStatus, state.trackStatus);
        return Container(
          alignment: Alignment.center,
          child: progbar,
        );
      } else if (dataStatus is SyncDataDone) {
        context.read<FileSyncBloc>().add(FileSyncRefreshLivData());
      }
      return Container(
          alignment: Alignment.topCenter,
          child: Column(mainAxisSize: MainAxisSize.min, children: [
            _buttonRow(),
            Row(mainAxisSize: MainAxisSize.min, children: children)
          ]));
    });
  }

  Widget _buttonRow() {
    return BlocBuilder<FileSyncBloc, FileSyncState>(builder: (context, state) {
      Container refreshButton = Container(
        padding: const EdgeInsets.all(16.0),
        child: ElevatedButton(
          onPressed: () {
            context.read<FileSyncBloc>().add(FileSyncRefreshLivData());
          },
          child: const Text('Refresh'),
        ),
      );
      bool syncActive = false;
      if (state.livDataStatus is DataRefreshed) {
        syncActive = true;
      }

      Container syncButton = (Container(
        padding: const EdgeInsets.all(16.0),
        child: ElevatedButton(
          onPressed: syncActive
              ? () {
                  context.read<FileSyncBloc>().add(FileSyncLivSyncRequested());
                }
              : null,
          child: const Text('Sync Liveries'),
        ),
      ));

      Container newModButton = Container(
        padding: const EdgeInsets.all(16.0),
        child: ElevatedButton(
          onPressed: () {
            if (state.canUpload) {
              Navigator.pushNamed(context, '/new_livery_aircraft');
            } else {
              Navigator.pushNamed(context, '/access_denied');
            }
          },
          child: const Text('Add an Aircraft'),
        ),
      );

      return Row(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: [refreshButton, syncButton, newModButton]);
    });
  }
}

class LiveryList extends StatefulWidget {
  const LiveryList({Key? key}) : super(key: key);

  @override
  LiveryListState createState() => LiveryListState();
}

class LiveryListState extends State<LiveryList> {
  @override
  Widget build(BuildContext context) {
    return BlocBuilder<FileSyncBloc, FileSyncState>(
      builder: (context, state) {
        if (state.livSyncData != null) {
          List<Widget> children = _aircraft(context, state);

          return ListView(children: children);
        } else {
          return Container();
        }
      },
    );
  }

  List<Widget> _aircraft(BuildContext context, FileSyncState state) {
    List<Widget> rows = [];
    dynamic data = state.livSyncData;
    bool canUpload = state.canUpload;
    bool colorblind = state.colorblindMode;
    for (String aircraft in data.keys) {
      rows.add(
          _buildRow(context, aircraft, data[aircraft], canUpload, colorblind));
    }
    return rows;
  }

  Color _headerColor(livs, colorblind) {
    bool tracked = false;
    for (ModuleModel liv in livs) {
      if (liv.tracked) {
        tracked = true;
      }
      if (liv.tracked && !liv.synced) {
        return X51Cols().getCol(colorblind, 'ko');
      }
    }
    if (tracked) {
      return X51Cols().getCol(colorblind, 'ok');
    }
    return X51Cols().getCol(colorblind, 'neutral');
  }

  Widget _buildRow(
      BuildContext context, aircraft, livs, canUpload, colorblind) {
    List<ListTile> elements = [];

    for (ModuleModel liv in livs) {
      elements.add(_buildSubRow(context, liv, canUpload, colorblind));
    }

    Color color = _headerColor(livs, colorblind);

    Widget trailing = canUpload
        ? _aircraftPopupMenu(context, aircraft, colorblind)
        : const SizedBox(height: 50, width: 50);
    return Flex(
      //height: 100,
      //height: 200,
      direction: Axis.vertical,
      //children: //ListView(
      children: [
        ExpandableGroup(
          isExpanded: false,
          header: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Text(aircraft,
                  style: TextStyle(fontWeight: FontWeight.bold, color: color)),
              trailing
            ],
          ),
          items: elements,
          headerEdgeInsets: const EdgeInsets.only(left: 16.0, right: 16.0),
        )
      ],
    );
  }

  ListTile _buildSubRow(
      BuildContext context, ModuleModel liv, bool canUpload, bool colorblind) {
    Icon icon = const Icon(Icons.explicit);
    if (liv.synced && liv.tracked) {
      icon = Icon(CupertinoIcons.checkmark_circle_fill,
          color: X51Cols().getCol(colorblind, 'ok'));
    } else if (liv.tracked && !liv.synced) {
      icon = Icon(CupertinoIcons.xmark_circle_fill,
          color: X51Cols().getCol(colorblind, 'ko'));
    } else if (!liv.tracked) {
      icon = Icon(CupertinoIcons.checkmark_circle_fill,
          color: X51Cols().getCol(colorblind, 'neutral'));
    }

    Widget? trailing = canUpload && liv.tracked
        ? _liveryPopupMenu(context, liv, colorblind)
        : const SizedBox(height: 50, width: 50);

    return ListTile(
      leading: icon,
      title: Text(liv.name),
      trailing: trailing,
    );
  }

  Widget? _liveryPopupMenu(
      BuildContext context, ModuleModel liv, bool colorblind) {
    List<PopupMenuEntry> choices = [
      PopupMenuItem(
        height: 50,
        value: LiveryPopupItemEdit(liv),
        child: SizedBox(
          height: 50,
          child: Row(
            //crossAxisAlignment: CrossAxisAlignment.stretch,
            children: [
              FaIcon(FontAwesomeIcons.solidPenToSquare,
                  color: X51Cols().getCol(colorblind, 'ok')),
              const Text('Edit'),
            ],
          ),
        ),
      ),
      PopupMenuItem(
        height: 50,
        value: LiveryPopupItemDelete(liv),
        child: SizedBox(
          height: 50,
          child: Row(
            //crossAxisAlignment: CrossAxisAlignment.stretch,
            children: [
              FaIcon(FontAwesomeIcons.trash,
                  color: X51Cols().getCol(colorblind, 'ko')),
              const Text('Un-track'),
            ],
          ),
        ),
      ),
    ];
    PopupMenuButton popup = PopupMenuButton(
      elevation: 3.2,
      initialValue: choices[1],
      onCanceled: () {},
      tooltip: 'Curator options',
      onSelected: (val) {
        _liveryPopupItemChoice(context, val);
      },
      itemBuilder: (BuildContext context) => choices,
      icon: const FaIcon(FontAwesomeIcons.ellipsisVertical),
    );
    return SizedBox(height: 50, width: 50, child: popup);
  }

  _liveryPopupItemChoice(context, LiveryPopupItem item) {
    ModuleModel liv = item.liv;

    if (item is LiveryPopupItemEdit) {
      //TODO
      BlocProvider.of<LiveryUploadBloc>(context)
          .add(LiveryUploadEditLiv(mod: liv));
      Navigator.of(context).push(MaterialPageRoute<NewLiveryView>(
          builder: (_) => BlocProvider.value(
              value: BlocProvider.of<LiveryUploadBloc>(context),
              child: const NewLiveryView())));
    } else if (item is LiveryPopupItemDelete) {
      _confirmUntrackLivery(context, liv);
    }
  }

  Widget _aircraftPopupMenu(BuildContext context, String aircraft, colorblind) {
    List<PopupMenuEntry> choices = [
      PopupMenuItem(
        height: 50,
        value: AircraftPopupItemEdit(aircraft),
        child: SizedBox(
          height: 50,
          child: Row(
            //crossAxisAlignment: CrossAxisAlignment.stretch,
            children: [
              FaIcon(FontAwesomeIcons.solidPenToSquare,
                  color: X51Cols().getCol(colorblind, 'ok')),
              const Text('Edit'),
            ],
          ),
        ),
      ),
      PopupMenuItem(
        height: 50,
        value: AircraftPopupItemDelete(aircraft),
        child: SizedBox(
          height: 50,
          child: Row(
            //crossAxisAlignment: CrossAxisAlignment.stretch,
            children: [
              FaIcon(FontAwesomeIcons.trash,
                  color: X51Cols().getCol(colorblind, 'ko')),
              const Text('Un-track'),
            ],
          ),
        ),
      ),
    ];
    PopupMenuButton popup = PopupMenuButton(
      elevation: 3.2,
      initialValue: choices[1],
      onCanceled: () {},
      tooltip: 'Curator options',
      onSelected: (val) {
        _aircraftPopupItemChoice(context, val);
      },
      itemBuilder: (BuildContext context) => choices,
      icon: const FaIcon(FontAwesomeIcons.ellipsisVertical),
    );
    return SizedBox(height: 50, width: 50, child: popup);
  }

  _aircraftPopupItemChoice(BuildContext context, AircraftPopupItem item) {
    String aircraft = item.aircraft;

    if (item is AircraftPopupItemEdit) {
      BlocProvider.of<LiveryUploadBloc>(context)
          .add(LiveryUploadEditAircraft(aircraft: aircraft));
      Navigator.of(context).push(MaterialPageRoute<NewLiveryView>(
          builder: (_) => BlocProvider.value(
              value: BlocProvider.of<LiveryUploadBloc>(context),
              child: const NewLiveryView())));
    } else if (item is AircraftPopupItemDelete) {
      _confirmUntrackAircraft(context, aircraft);
    }
  }

  void _confirmUntrackAircraft(BuildContext context, String aircraft) {
    Widget okButton = TextButton(
      child: const Text('CONFIRM'),
      onPressed: () {
        context
            .read<LiveryUploadBloc>()
            .add(LiveryUploadDeleteAircraft(aircraft: aircraft));
        Navigator.pop(context);
      },
    );

    Widget cancelButton = TextButton(
      child: const Text('CANCEL'),
      onPressed: () {
        Navigator.pop(context);
      },
    );

    // set up the AlertDialog
    AlertDialog dialog = AlertDialog(
        actions: [cancelButton, okButton],
        actionsAlignment: MainAxisAlignment.spaceAround,
        content: SizedBox(
          height: 150,
          width: 200,
          child: Column(
            children: [
              Container(
                  padding: const EdgeInsets.all(5),
                  child: const Icon(Icons.warning_amber_outlined,
                      size: 60, color: Colors.orange)),
              Text(
                'Are you sure you want to remove $aircraft and all the liveries it contains from ModSync?',
                textAlign: TextAlign.center,
              ),
            ],
          ),
        ));

    showDialog(
        context: context,
        builder: (BuildContext context) {
          return dialog;
        });
  }

  void _confirmUntrackLivery(BuildContext context, ModuleModel liv) {
    Widget okButton = TextButton(
      child: const Text('CONFIRM'),
      onPressed: () {
        context.read<LiveryUploadBloc>().add(LiveryUploadDeleteLiv(mod: liv));
        Navigator.pop(context);
      },
    );

    Widget cancelButton = TextButton(
      child: const Text('CANCEL'),
      onPressed: () {
        Navigator.pop(context);
      },
    );

    // set up the AlertDialog
    AlertDialog dialog = AlertDialog(
        actions: [cancelButton, okButton],
        actionsAlignment: MainAxisAlignment.spaceAround,
        content: SizedBox(
          height: 150,
          width: 200,
          child: Column(
            children: [
              Container(
                  padding: const EdgeInsets.all(5),
                  child: const Icon(Icons.warning_amber_outlined,
                      size: 60, color: Colors.orange)),
              Text(
                'Are you sure you want to remove the livery ${liv.name} from ${liv.parent}?',
                textAlign: TextAlign.center,
              ),
            ],
          ),
        ));

    showDialog(
        context: context,
        builder: (BuildContext context) {
          return dialog;
        });
  }
}
