import 'package:flutter/material.dart';

class UploadCompleteView extends StatelessWidget {
  const UploadCompleteView({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Card(
        child: Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        const Icon(Icons.check_circle_outlined,
            size: 40, color: Colors.tealAccent),
        const Text('Upload Complete'),
        ElevatedButton(
            onPressed: () {
              Navigator.popAndPushNamed(context, 'edit_mod');
            },
            child: const Text('OK'))
      ],
    ));
  }
}
