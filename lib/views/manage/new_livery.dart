import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_phoenix/flutter_phoenix.dart';
import 'package:x51sync/src/livery_upload/bloc/liveryupload_bloc.dart';
import 'package:x51sync/src/livery_upload/bloc/liveryupload_event.dart';
import 'package:x51sync/src/livery_upload/bloc/liveryupload_state.dart';
import 'package:x51sync/src/services/api_state.dart';
import 'package:x51sync/src/session/bloc/session_bloc.dart';
import 'package:x51sync/src/transfer/transfer_status.dart';
import 'package:x51sync/views/widgets/api_error_handler.dart';
import 'package:x51sync/views/widgets/livery_upload.dart';
import 'package:x51sync/views/widgets/log_exporter.dart';

class NewLiveryView extends StatelessWidget {
  const NewLiveryView({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocListener<SessionBloc, SessionState>(
      listener: (context, state) {
        checkApiErrors(context, state);
      },
      child: BlocBuilder<LiveryUploadBloc, LiveryUploadState>(
        builder: (context, state) {
          bool implyLeading = true;
          if (state.uploadStatus is TransferringFiles) {
            implyLeading = false;
          }
          return Scaffold(
            appBar: AppBar(
              title: const Text('Upload Livery'),
              actions: [exportButton(context)],
              automaticallyImplyLeading: implyLeading,
            ),
            body: Stack(children: const [LiveryUploadForm()]),
          );
        },
      ),
    );
  }
}

class LiveryUploadForm extends StatelessWidget {
  const LiveryUploadForm({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocListener<LiveryUploadBloc, LiveryUploadState>(
      listener: (context, state) {
        final uploadStatus = state.uploadStatus;
        if (uploadStatus is TransferFailed) {
          _showSnackBar(context, uploadStatus.exception.toString());
        } else if (uploadStatus is ApiAuthExpired) {
          Phoenix.rebirth(context);
        } else if (uploadStatus is TransferComplete) {
          _successfulSubmission(context);
        }
      },
      child: BlocBuilder<LiveryUploadBloc, LiveryUploadState>(
          builder: (context, state) {
        //if (state.uploadStatus is ) {
        //  return Container();
        // }
        if (state.mod != null || state.isBatch) {
          return _editLivery(context, state);
        }
        return Column(
            children: [Text(state.mod!.parent), Text(state.mod!.name)]);
      }),
    );
  }

  Widget _folderPicker(context, state) {
    return BlocBuilder<LiveryUploadBloc, LiveryUploadState>(
      builder: (context, state) {
        if (state.uploadStatus is! TransferringFiles) {
          return const LivUploadPicker();
        } else {
          return Container();
        }
      },
    );
  }

  Widget _submitButton(container) {
    return BlocBuilder<LiveryUploadBloc, LiveryUploadState>(
        builder: (context, state) {
      bool ready = state.ready;
      //String modType = state.modType;
      if (state.uploadStatus is TransferringFiles) {
        return Container(
            padding: const EdgeInsets.all(16.0),
            child: const CircularProgressIndicator());
      } else if (state.uploadStatus is TransferComplete) {
        return Container(
            padding: const EdgeInsets.all(16.0),
            child: const Icon(Icons.check_circle_rounded,
                color: Colors.green, size: 15));
      } else if (ready) {
        return Container(
          padding: const EdgeInsets.all(16.0),
          child: ElevatedButton(
            onPressed: () {
              context.read<LiveryUploadBloc>().add(LiveryUploadLivSubmitted());
            },
            child: const Text('Upload Livery'),
          ),
        );
      } else {
        return Container();
      }
    });
  }

  Widget _editLivery(context, state) {
    String title = '';
    if (state.isBatch) {
      title = 'Submitting batch of liveries for  ${state.aircraft}';
    } else {
      title =
          'Submitting new version of ${state.mod.name} for ${state.mod.parent}';
    }
    return Container(
      padding: const EdgeInsets.all(16),
      alignment: Alignment.center,
      child: Card(
        child: Column(
          children: [
            const SizedBox(height: 20),
            Text(title),
            const SizedBox(height: 20),
            _folderPicker(state, context),
            const SizedBox(height: 20),
            _submitButton(Container()),
            const SizedBox(height: 20),
            _uploadStatus(context),
          ],
        ),
      ),
    );
  }

  Widget _uploadStatus(context) {
    return BlocBuilder<LiveryUploadBloc, LiveryUploadState>(
        builder: (context, state) {
      TransferStatus procState = state.uploadStatus;
      if (procState is ProcessingFiles) {
        return Column(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: const [
              CircularProgressIndicator(),
              Text("Calculating...")
            ]);
      } else if (procState is ProcessingComplete) {
        String sizeStr = state.sizeStr;
        //int nFiles = state.uploadFiles!.nFiles();
        //return Text("Mod Size: $sizeStr ($nFiles files)");
        return Text("Upload Size: $sizeStr");
      } else if (procState is ProcessingFailed) {
        String error = procState.exception.toString();
        return Text('Error calculating size: $error');
      } else if (procState is TransferringFiles) {
        return _uploadPanel(procState);
      } else {
        return const Text('-');
      }
    });
  }

  _uploadPanel(TransferringFiles dstate) {
    return BlocBuilder<LiveryUploadBloc, LiveryUploadState>(
        builder: (context, state) {
      String title = 'Uploading...';
      String subtitle = 'Total:';

      double totbarVal = dstate.totPercent;
      String totBarLabel =
          '${dstate.sentBytes}/${dstate.totalBytes}: (${dstate.filesSent}/${dstate.totalFiles})';

      //String file = dstate.currFile;
      LinearProgressIndicator totBar = LinearProgressIndicator(
        value: totbarVal,
        valueColor: const AlwaysStoppedAnimation<Color>(Colors.yellow),
        semanticsLabel: totBarLabel,
      );

      List<Widget> fileBars = [];
      for (var track in dstate.tracks) {
        String fName = track.file;
        double fileBarVal = track.filePercent;
        String fileBarlabel = '${track.fileBytesSent}/${track.fileBytesTot}';
        LinearProgressIndicator fileBar = LinearProgressIndicator(
          value: fileBarVal,
          valueColor: const AlwaysStoppedAnimation<Color>(Colors.yellow),
          semanticsLabel: fileBarlabel,
        );

        Text fNametext = Text(fName,
            style: const TextStyle(color: Colors.blueGrey, fontSize: 12));
        Text fileBarlabelText = Text(fileBarlabel);
        fileBars.addAll([fileBar, fNametext, fileBarlabelText]);
      }

      List<Widget> children = [
        Text(title),
        const Divider(),
        Text(totBarLabel),
        totBar,
        const Divider()
      ];
      children.addAll(fileBars);
      return Container(
          padding: const EdgeInsets.all(100),
          child: Column(children: children));
    });
  }

  void _successfulSubmission(context) {
    Widget okButton = TextButton(
      child: const Text('OK'),
      onPressed: () {
        //context.read<LiveryUploadBloc>().add(LiveryUploadInit());
        Navigator.popUntil(context, ModalRoute.withName('/liveries'));

        //Navigator.pushReplacement(
        //context, MaterialPageRoute(builder: (context) => EditModView()));
      },
    );
    // set up the AlertDialog
    AlertDialog dialog = AlertDialog(
        actions: [okButton],
        content: SizedBox(
          height: 100,
          width: 200,
          child: Column(
            children: const [
              Icon(Icons.check_circle_outlined,
                  size: 40, color: Colors.tealAccent),
              Text('Upload Complete'),
            ],
          ),
        ));

    showDialog(
        context: context,
        builder: (BuildContext context) {
          return dialog;
        });
  }

  void _showSnackBar(BuildContext context, String message) {
    //error message snackbar
    final snackBar = SnackBar(content: Text(message));
    ScaffoldMessenger.of(context).showSnackBar(snackBar);
    context.read<LiveryUploadBloc>().add(LiveryUploadInit());
  }
}
