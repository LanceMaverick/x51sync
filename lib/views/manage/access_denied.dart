import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:x51sync/views/widgets/log_exporter.dart';

class AccessDeniedView extends StatelessWidget {
  const AccessDeniedView({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
            title: const Text('DENIED'), actions: [exportButton(context)]),
        body: Card(
          child: SizedBox(
            height: 200,
            child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: const [
                  Center(
                    child: FaIcon(
                      FontAwesomeIcons.triangleExclamation,
                      size: 60,
                      color: Colors.orange,
                    ),
                  ),
                  Text("You are not authorised to access this functionality.",
                      style: TextStyle(color: Colors.red, fontSize: 20))
                ]),
          ),
        ));
  }
}
