import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:x51sync/src/file_sync/data_state.dart';
import 'package:x51sync/src/file_sync/bloc/files_bloc.dart';
import 'package:x51sync/src/file_sync/bloc/files_event.dart';
import 'package:x51sync/src/file_sync/bloc/files_state.dart';
import 'package:x51sync/src/session/bloc/session_bloc.dart';
import 'package:x51sync/views/widgets/api_error_handler.dart';
import 'package:x51sync/views/widgets/lister.dart';
import 'package:x51sync/views/widgets/log_exporter.dart';
import 'package:x51sync/views/widgets/sync_panel.dart';

class ModsView extends StatelessWidget {
  const ModsView({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocListener<SessionBloc, SessionState>(
      listener: (context, state) {
        checkApiErrors(context, state);
      },
      child:
          BlocBuilder<FileSyncBloc, FileSyncState>(builder: (context, state) {
        //context.read<FileSyncBloc>().add(FileSyncInit());
        return Scaffold(
          appBar: AppBar(
              title: const Text('Mods'), actions: [exportButton(context)]),
          body: Stack(
            children: const [ModList()],
          ),
        );
      }),
    );
  }
}

class ModList extends StatelessWidget {
  const ModList({Key? key}) : super(key: key);

  Widget _wrappedList(list, title) {
    //TODO: READD

    return Column(mainAxisSize: MainAxisSize.min, children: [
      Text(title, style: const TextStyle(fontSize: 30)),
      //const Divider(),
      Card(
          elevation: 10,
          shape:
              RoundedRectangleBorder(borderRadius: BorderRadius.circular(20)),
          child: SizedBox(height: 500.0, width: 600, child: list))
    ]);
  }

  Widget _buttonRow(context) {
    return BlocBuilder<FileSyncBloc, FileSyncState>(builder: (context, state) {
      Container refreshButton = Container(
        padding: const EdgeInsets.all(16.0),
        child: ElevatedButton(
          onPressed: () {
            context.read<FileSyncBloc>().add(FileSyncRefreshModData());
          },
          child: const Text('Refresh'),
        ),
      );
      bool syncActive = false;
      if (state.modDataStatus is DataRefreshed) {
        syncActive = true;
      }

      Container syncButton = (Container(
        padding: const EdgeInsets.all(16.0),
        child: ElevatedButton(
          onPressed: syncActive
              ? () {
                  context.read<FileSyncBloc>().add(FileSyncModSyncRequested());
                }
              : null,
          child: const Text('Sync Mods'),
        ),
      ));

      Container newModButton = Container(
        padding: const EdgeInsets.all(16.0),
        child: ElevatedButton(
          onPressed: () {
            if (!state.canUpload) {
              Navigator.pushNamed(context, '/access_denied');
            } else {
              Navigator.pushNamed(context, '/edit_mod');
            }
          },
          child: const Text('Update/Add Mod'),
        ),
      );

      return Row(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: [refreshButton, syncButton, newModButton]);
    });
  }

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<FileSyncBloc, FileSyncState>(builder: (context, state) {
      List<Widget> children = [];
      final dataStatus = state.modDataStatus;
      if (dataStatus is DataRefreshing) {
        String msg = "Refreshing data...";
        if (dataStatus is RemoteDataRefreshing) {
          msg = "Checking remote files...";
        } else if (dataStatus is SyncDataRefreshing) {
          msg = "Generating deltas...";
        }
        SpinKitCubeGrid spinkit = SpinKitCubeGrid(
          color: Theme.of(context).colorScheme.primary,
          size: 200.0,
        );
        children.add(Container(
          padding: const EdgeInsets.all(32.0),
          alignment: Alignment.center,
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: [
              Padding(
                  padding: const EdgeInsets.all(200),
                  child: Column(
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      children: [Text(msg), spinkit])),
            ],
          ),
        ));
      } else if (dataStatus is InitialDataStatus) {
        children.add(Container(
            alignment: Alignment.center,
            child: const Text('Click refresh to see sync status')));
      } else if (dataStatus is DataRefreshFailed) {
        Object error = dataStatus.exception;
        String err = '';
        //TODO - move to bloc
        if (error is FileSystemException) {
          err =
              'Make sure you DCS saved games folder is set correctly, and all mod folders are present';
        } else {
          err = error.toString();
        }
        //TODO: use exception type
        //if (err.contains('401')) {
        //  context.read<FileSyncBloc>().add(FileSyncInit());
        //  return showExpired(context);
        //}
        children.add(Container(
            alignment: Alignment.center,
            child: Text('Error retrieving data: $err')));
      } else if (dataStatus is DataRefreshed) {
        for (var key in state.modSyncData!.keys) {
          var data = state.modSyncData![key];
          children.add(_wrappedList(FileLister(data: data, title: key), key));
        }
      } else if (dataStatus is DataSyncing) {
        Widget progbar = syncPanel(state.syncStatus, state.trackStatus);
        return Container(
          alignment: Alignment.center,
          child: progbar,
        );
      } else if (dataStatus is SyncDataDone) {
        context.read<FileSyncBloc>().add(FileSyncRefreshModData());
      }
      return Container(
          alignment: Alignment.topCenter,
          child: Column(mainAxisSize: MainAxisSize.min, children: [
            _buttonRow(context),
            Row(mainAxisSize: MainAxisSize.min, children: children)
          ]));
    });
  }
}
