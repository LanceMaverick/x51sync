abstract class FileStatus {
  const FileStatus();
}

class InitialFileStatus extends FileStatus {
  const InitialFileStatus();
}

class GettingLocalStatus extends FileStatus {}

class GettingRemoteStatus extends FileStatus {}

class GettingFailed extends FileStatus {
  final Object exception;

  GettingFailed(this.exception);
}
