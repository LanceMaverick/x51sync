import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_phoenix/flutter_phoenix.dart';
import 'package:x51sync/src/services/api_state.dart';
import 'package:x51sync/src/session/bloc/session_bloc.dart';
import 'package:x51sync/src/transfer/transfer_status.dart';
import 'package:x51sync/src/services/api_service.dart';
import 'package:x51sync/src/services/file_service.dart';
import 'package:x51sync/src/file_upload/bloc/fileupload_event.dart';
import 'package:x51sync/src/file_upload/bloc/fileupload_state.dart';
import 'package:x51sync/src/file_upload/bloc/fileupload_bloc.dart';
import 'package:x51sync/src/services/sync_service.dart';
import 'package:x51sync/views/manage/new_mod.dart';
import 'package:x51sync/views/widgets/api_error_handler.dart';
import 'package:x51sync/views/widgets/log_exporter.dart';

class EditModView extends StatelessWidget {
  EditModView({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocListener<SessionBloc, SessionState>(
      listener: (context, state) {
        checkApiErrors(context, state);
      },
      child: Scaffold(
        appBar: AppBar(
            title: const Text('Update or add a new mod'),
            actions: [exportButton(context)]),
        backgroundColor: Colors.grey.shade100,
        body: BlocProvider(
            create: (_) => FileuploadBloc(
                apiSvc: RepositoryProvider.of<ApiService>(context),
                fileSvc: FileService(),
                syncSvc: SyncService())
              ..add(FileuploadInit()),
            child: _home()),
      ),
    );
  }

  Widget _home() {
    return BlocListener<FileuploadBloc, FileuploadState>(
      listener: (context, state) {
        if (state.uploadStatus is ApiAuthExpired) {
          Phoenix.rebirth(context);
        }
      },
      child: BlocBuilder<FileuploadBloc, FileuploadState>(
        builder: (context, state) {
          //if (state.uploadStatus is TransferNoPrivelages) {
          //  WidgetsBinding.instance?.addPostFrameCallback((_) {
          //    Navigator.of(context).pushReplacementNamed('/access_denied');
          //  });
          //}
          return Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Container(child: _newMod()),
              const Divider(),
              const Text('Current tracked  mods'),
              const Divider(),
              const SizedBox(width: 1000, height: 400, child: UpdateModList()),
            ],
          );
        },
      ),
    );
  }

  final Widget _description = Column(children: [
    Container(
        padding: const EdgeInsets.all(5),
        child: const Text(
            'Choose a mod type and either select an existing mod from the list to update, or click the button below to upload a new mod')),
    const Divider()
  ]);

  Widget _newMod() {
    /*  return BlocListener<FileuploadBloc, FileuploadState>(listener:
        (context, state) {
      final formStatus = state.formStatus;
      if (formStatus is SubmissionFailed) {
        _showSnackBar(context, formStatus.exception.toString());
      } else if (formStatus is SubmissionSuccess) {
        _successfulSubmission(context);
      }
    }, child:*/
    return BlocBuilder<FileuploadBloc, FileuploadState>(
        builder: (context, state) {
      return Padding(
          padding: const EdgeInsets.symmetric(horizontal: 10),
          child: Card(
            elevation: 5,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                _description,
                Row(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: [_typeField(), _newModButton(context)])
                //padding: const EdgeInsets.fromLTRB(20, 5, 20, 20))),
                //const SizedBox(height: 30),
              ],
            ),
          ));
    });
  }

  Widget _newModButton(context) {
    return BlocBuilder<FileuploadBloc, FileuploadState>(
        builder: (context, state) {
      String modType = state.modType;
      return Container(
        padding: const EdgeInsets.all(16.0),
        child: ElevatedButton(
          onPressed: state.uploadStatus == const InitialTransferStatus()
              ? () {
                  context
                      .read<FileuploadBloc>()
                      .add(FileuploadNewMod(modType: state.modType));
                  navigateNewMod(context);
                }
              : null,
          child: Text('Add New $modType mod'),
        ),
      );
    });
  }

  Widget _typeField() {
    return BlocBuilder<FileuploadBloc, FileuploadState>(
        builder: (context, state) {
      String dropdownValue = state.modType;
      return Row(children: [
        const Text('Mod type:'),
        const SizedBox(width: 10),
        DropdownButton<String>(
            value: dropdownValue,
            icon:
                Icon(Icons.arrow_drop_down_circle, color: Colors.red.shade900),
            iconSize: 24,
            elevation: 20,
            underline: Container(height: 3),
            onChanged: (value) {
              dropdownValue = value!;
              context
                  .read<FileuploadBloc>()
                  .add(FileuploadModTypeChanged(modType: value));
            },
            items: state.modDirs.map<DropdownMenuItem<String>>((String item) {
              return DropdownMenuItem(
                value: item,
                child: Text(item),
              );
            }).toList())
      ]);
    });
  }
}

class UpdateModList extends StatefulWidget {
  const UpdateModList({Key? key}) : super(key: key);

  @override
  UpdateModListState createState() => UpdateModListState();
}

class UpdateModListState extends State<UpdateModList> {
  @override
  Widget build(BuildContext context) {
    return BlocBuilder<FileuploadBloc, FileuploadState>(
        builder: (context, state) {
      if (state.uploadStatus is GettingModList) {
        return Container(
            padding: const EdgeInsets.all(16),
            alignment: Alignment.center,
            child: Column(children: const [
              Text('Retrieving tracked mod list...'),
              CircularProgressIndicator()
            ]));
      }
      final mods = state.selectedMods;

      return ListView.builder(
          padding: const EdgeInsets.all(16.0),
          itemCount: mods.length,
          itemBuilder: (context, int i) {
            return _buildRow(mods[i]);
          });
    });
  }

  Widget _buildRow(mod) {
    return BlocBuilder<FileuploadBloc, FileuploadState>(
      builder: (context, state) {
        return Card(
          elevation: 10,
          child: InkWell(
            onTap: () {
              context.read<FileuploadBloc>().add(FileuploadEditMod(mod: mod));
              navigateNewMod(context);
            },
            child: Row(mainAxisAlignment: MainAxisAlignment.center, children: [
              SizedBox(
                height: 50,
                child: Center(
                  child: Text(mod.name),
                ),
              ),
            ]),
          ),
        );
      },
    );
  }
}

void navigateNewMod(context) {
  Navigator.of(context).push(MaterialPageRoute<NewModView>(
    builder: (_) => BlocProvider.value(
        value: BlocProvider.of<FileuploadBloc>(context), child: NewModView()),
  ));
}
