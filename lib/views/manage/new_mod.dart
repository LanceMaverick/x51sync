import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_phoenix/flutter_phoenix.dart';
import 'package:x51sync/models/files.dart';
import 'package:x51sync/src/file_upload/bloc/fileupload_bloc.dart';
import 'package:x51sync/src/file_upload/bloc/fileupload_event.dart';
import 'package:x51sync/src/file_upload/bloc/fileupload_state.dart';
import 'package:x51sync/src/services/api_state.dart';
import 'package:x51sync/src/session/bloc/session_bloc.dart';
import 'package:x51sync/src/transfer/transfer_status.dart';
import 'package:x51sync/views/widgets/api_error_handler.dart';
import 'package:x51sync/views/widgets/log_exporter.dart';
import 'package:x51sync/views/widgets/mod_upload.dart';

class NewModView extends StatelessWidget {
  NewModView({Key? key}) : super(key: key);
  final _formKey = GlobalKey<FormState>();

  @override
  Widget build(BuildContext context) {
    return BlocListener<SessionBloc, SessionState>(
      listener: (context, state) {
        checkApiErrors(context, state);
      },
      child: BlocBuilder<FileuploadBloc, FileuploadState>(
        builder: (context, state) {
          bool implyLeading = true;
          if (state.uploadStatus is TransferringFiles) {
            implyLeading = false;
          }
          return Scaffold(
              appBar: AppBar(
                  title: const Text('Update or add a new mod'),
                  automaticallyImplyLeading: implyLeading,
                  actions: [exportButton(context)]),
              body: _newModForm());
        },
      ),
    );
  }

  Widget _newModForm() {
    return BlocListener<FileuploadBloc, FileuploadState>(listener:
        (context, state) {
      final uploadStatus = state.uploadStatus;
      if (uploadStatus is TransferFailed) {
        _showSnackBar(context, uploadStatus.exception.toString());
      } else if (uploadStatus is ApiAuthExpired) {
        Phoenix.rebirth(context);
      } else if (uploadStatus is TransferComplete) {
        _successfulSubmission(context);
      }
    }, child:
        BlocBuilder<FileuploadBloc, FileuploadState>(builder: (context, state) {
      return Form(
        key: _formKey,
        child: Padding(
          padding: const EdgeInsets.symmetric(horizontal: 40),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              _deleteButton(context, state),
              _nameField(),
              const SizedBox(height: 20),
              _pathField(context),
              const SizedBox(height: 20),
              _uploadStatus(context),
              const SizedBox(height: 40),
              _submitButton(context)
            ],
          ),
        ),
      );
    }));
  }

  Widget _deleteButton(context, state) {
    if (state.newMod) {
      return Container();
    } else {
      return Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          ElevatedButton(
            child: const Text('Delete Mod'),
            onPressed: () {
              _confirmUntrackMod(context, state.mod!, state.modType);
            },
          ),
        ],
      );
    }
  }

  Widget _nameField() {
    return BlocBuilder<FileuploadBloc, FileuploadState>(
        builder: (context, state) {
      if (state.newMod && state.uploadStatus is! TransferringFiles) {
        return TextFormField(
          decoration: const InputDecoration(
            icon: Icon(Icons.edit),
            hintText: "Name of the Mod",
          ),
          validator: (value) => state.isValidmodName
              ? null
              : 'Mod name is too short or already exists',
          onChanged: (value) => context
              .read<FileuploadBloc>()
              .add(FileuploadModNameChanged(modName: value)),
        );
      } else {
        String modName = '';
        if (state.newMod) {
          modName = state.modName;
        } else {
          modName = state.mod!.name;
        }
        return TextFormField(
          decoration: const InputDecoration(
            icon: Icon(Icons.edit),
            labelText: 'Submitting a new version of:',
            hintText: "Name of the Mod",
          ),
          validator: (value) =>
              state.isValidmodName ? null : 'Mod name is too short',
          onChanged: (value) {},
          readOnly: true,
          enabled: false,
          initialValue: modName,
        );
      }
    });
  }

  Widget _pathField(context) {
    return BlocBuilder<FileuploadBloc, FileuploadState>(
        builder: (context, state) {
      if (state.uploadStatus is! TransferringFiles) {
        return ModUploadPickerWidget();
      } else {
        return Card(
            child: (Container(
                padding: const EdgeInsets.all(5), child: Text(state.path))));
      }
    });
  }

  Widget _submitButton(container) {
    return BlocBuilder<FileuploadBloc, FileuploadState>(
        builder: (context, state) {
      bool ready = state.ready;
      //String modType = state.modType;
      if (state.uploadStatus is TransferringFiles) {
        return Container(
            padding: const EdgeInsets.all(16.0),
            child: const CircularProgressIndicator());
      } else if (state.uploadStatus is TransferComplete) {
        return Container(
            padding: const EdgeInsets.all(16.0),
            child: const Icon(Icons.check_circle_rounded,
                color: Colors.green, size: 15));
      } else if (ready) {
        return Container(
          padding: const EdgeInsets.all(16.0),
          child: ElevatedButton(
            onPressed: () {
              context.read<FileuploadBloc>().add(FileuploadModSubmitted());
            },
            child: const Text('Upload Mod'),
          ),
        );
      } else {
        return Container();
      }
    });
  }

  Widget _uploadStatus(context) {
    return BlocBuilder<FileuploadBloc, FileuploadState>(
        builder: (context, state) {
      TransferStatus procState = state.uploadStatus;
      if (procState is ProcessingFiles) {
        return Column(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: const [
              CircularProgressIndicator(),
              Text("Calculating...")
            ]);
      } else if (procState is ProcessingComplete) {
        String sizeStr = state.sizeStr;
        //int nFiles = state.uploadFiles!.nFiles();
        //return Text("Mod Size: $sizeStr ($nFiles files)");
        return Text("Upload Size: $sizeStr");
      } else if (procState is ProcessingFailed) {
        String error = procState.exception.toString();
        return Text('Error calculating size: $error');
      } else if (procState is TransferringFiles) {
        return _uploadPanel(procState);
      } else {
        return const Text('-');
      }
    });
  }

  void _successfulSubmission(context) {
    Widget okButton = TextButton(
      child: const Text('OK'),
      onPressed: () {
        Navigator.popUntil(context, ModalRoute.withName('/mods'));

        //Navigator.pushReplacement(
        //context, MaterialPageRoute(builder: (context) => EditModView()));
      },
    );
    // set up the AlertDialog
    AlertDialog dialog = AlertDialog(
        actions: [okButton],
        content: SizedBox(
          height: 100,
          width: 200,
          child: Column(
            children: const [
              Icon(Icons.check_circle_outlined,
                  size: 40, color: Colors.tealAccent),
              Text('Upload Complete'),
            ],
          ),
        ));

    showDialog(
        context: context,
        builder: (BuildContext context) {
          return dialog;
        });
  }

  void _confirmUntrackMod(
      BuildContext context, ModuleModel mod, String modType) {
    Widget okButton = TextButton(
      child: const Text('CONFIRM'),
      onPressed: () {
        context
            .read<FileuploadBloc>()
            .add(FileuploadDeleteMod(mod: mod, modType: modType));
        Navigator.popUntil(context, ModalRoute.withName('/mods'));
      },
    );

    Widget cancelButton = TextButton(
      child: const Text('CANCEL'),
      onPressed: () {
        Navigator.pop(context);
      },
    );
    AlertDialog dialog = AlertDialog(
        actions: [cancelButton, okButton],
        actionsAlignment: MainAxisAlignment.spaceAround,
        content: SizedBox(
          height: 150,
          width: 200,
          child: Column(
            children: [
              Container(
                  padding: const EdgeInsets.all(5),
                  child: const Icon(Icons.warning_amber_outlined,
                      size: 60, color: Colors.orange)),
              Text(
                'Are you sure you want to remove ${mod.name} from ModSync?',
                textAlign: TextAlign.center,
              ),
            ],
          ),
        ));
    showDialog(
        context: context,
        builder: (BuildContext context) {
          return dialog;
        });
  }

  void _showSnackBar(BuildContext context, String message) {
    //error message snackbar
    final snackBar = SnackBar(content: Text(message));
    ScaffoldMessenger.of(context).showSnackBar(snackBar);
    context.read<FileuploadBloc>().add(FileuploadInit());
  }

  _uploadPanel(TransferringFiles dstate) {
    return BlocBuilder<FileuploadBloc, FileuploadState>(
        builder: (context, state) {
      String title = 'Uploading...';
      String subtitle = 'Total:';

      double totbarVal = dstate.totPercent;
      String totBarLabel =
          '${dstate.sentBytes}/${dstate.totalBytes}: (${dstate.filesSent}/${dstate.totalFiles})';

      //String file = dstate.currFile;
      LinearProgressIndicator totBar = LinearProgressIndicator(
        value: totbarVal,
        valueColor: const AlwaysStoppedAnimation<Color>(Colors.yellow),
        semanticsLabel: totBarLabel,
      );

      List<Widget> fileBars = [];
      for (var track in dstate.tracks) {
        String fName = track.file;
        double fileBarVal = track.filePercent;
        String fileBarlabel = '${track.fileBytesSent}/${track.fileBytesTot}';
        LinearProgressIndicator fileBar = LinearProgressIndicator(
          value: fileBarVal,
          valueColor: const AlwaysStoppedAnimation<Color>(Colors.yellow),
          semanticsLabel: fileBarlabel,
        );

        Text fNametext = Text(fName,
            style: const TextStyle(color: Colors.blueGrey, fontSize: 12));
        Text fileBarlabelText = Text(fileBarlabel);
        fileBars.addAll([fileBar, fNametext, fileBarlabelText]);
      }

      List<Widget> children = [
        Text(title),
        const Divider(),
        Text(totBarLabel),
        totBar,
        const Divider()
      ];
      children.addAll(fileBars);
      return Container(
          padding: const EdgeInsets.all(100),
          child: Column(children: children));
    });
  }
}
