import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:x51sync/src/livery_upload/bloc/liveryupload_bloc.dart';
import 'package:x51sync/src/livery_upload/bloc/liveryupload_event.dart';
import 'package:x51sync/src/livery_upload/bloc/liveryupload_state.dart';
import 'package:x51sync/src/session/bloc/session_bloc.dart';
import 'package:x51sync/src/transfer/transfer_status.dart';
import 'package:x51sync/views/manage/new_livery.dart';
import 'package:x51sync/views/widgets/api_error_handler.dart';
import 'package:x51sync/views/widgets/log_exporter.dart';

class NewAircraftView extends StatelessWidget {
  NewAircraftView({Key? key}) : super(key: key);
  final _formKey = GlobalKey<FormState>();

  @override
  Widget build(BuildContext context) {
    return BlocListener<SessionBloc, SessionState>(
      listener: (context, state) {
        checkApiErrors(context, state);
      },
      child: BlocBuilder<LiveryUploadBloc, LiveryUploadState>(
        builder: (context, state) {
          //bool implyLeading = true;
          //if (state.uploadStatus is TransferringFiles) {
          //  implyLeading = false;
          // }
          return Scaffold(
            appBar: AppBar(
                title: const Text('New Aircraft'),
                actions: [exportButton(context)]),
            body: BlocBuilder<LiveryUploadBloc, LiveryUploadState>(
                builder: (context, state) {
              return _nameForm(context);
            }),
          );
        },
      ),
    );
  }

  Widget _nameForm(context) {
    return BlocListener<LiveryUploadBloc, LiveryUploadState>(
        listener: (context, state) {
          final uploadStatus = state.uploadStatus;
          if (uploadStatus is TransferFailed) {
            _showSnackBar(context, uploadStatus.exception.toString());
          }
        },
        child: Form(
          key: _formKey,
          child: Padding(
            padding: const EdgeInsets.symmetric(horizontal: 40),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                const Text(
                    'Name must be entered EXACTLY as required for liveries of this aircraft (e.g "FA-18C_hornet")'),
                const SizedBox(height: 20),
                _nameField(),
                _submitButton(),
              ],
            ),
          ),
        ));
  }

  Widget _nameField() {
    return BlocBuilder<LiveryUploadBloc, LiveryUploadState>(
        builder: (context, state) {
      return TextFormField(
        decoration: const InputDecoration(
          icon: Icon(Icons.airplanemode_active),
          hintText: 'Aircraft folder name',
        ),
        validator: (value) =>
            state.isValidmodDir ? state.parentFieldCheck() : null,
        onChanged: (value) => context.read<LiveryUploadBloc>().add(
              LiveryUploadAircraftNameChanged(aircraft: value),
            ),
      );
    });
  }

  Widget _submitButton() {
    return BlocBuilder<LiveryUploadBloc, LiveryUploadState>(
      builder: (context, state) {
        return ElevatedButton(
          onPressed: () {
            if (_formKey.currentState!.validate()) {
              BlocProvider.of<LiveryUploadBloc>(context)
                  .add(LiveryUploadEditAircraft(aircraft: state.aircraft));
              Navigator.of(context).push(MaterialPageRoute<NewLiveryView>(
                  builder: (_) => BlocProvider.value(
                      value: BlocProvider.of<LiveryUploadBloc>(context),
                      child: const NewLiveryView())));
            }
          },
          child: const Text('OK'),
        );
      },
    );
  }

  void _showSnackBar(BuildContext context, String message) {
    //error message snackbar
    final snackBar = SnackBar(content: Text(message));
    ScaffoldMessenger.of(context).showSnackBar(snackBar);
    context.read<LiveryUploadBloc>().add(LiveryUploadInit());
  }
}
