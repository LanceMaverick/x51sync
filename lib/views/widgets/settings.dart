import 'package:filepicker_windows/filepicker_windows.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:x51sync/src/file_sync/bloc/files_bloc.dart';
import 'package:x51sync/src/file_sync/bloc/files_event.dart';
import 'package:x51sync/src/file_sync/bloc/files_state.dart';
import 'package:x51sync/tools/window.dart';

class Folder extends StatefulWidget {
  const Folder({Key? key}) : super(key: key);

  @override
  FolderState createState() => FolderState();
}

class FolderState extends State<Folder> {
  @override
  Widget build(BuildContext context) {
    Text text;
    return BlocBuilder<FileSyncBloc, FileSyncState>(builder: (context, state) {
      if (state.root == '' || state.root == null) {
        text = const Text('No folder set', style: TextStyle(color: Colors.red));
      } else {
        text = Text(state.root!);
      }
      return Column(children: [_buildFolderWidget(context, text)]);
    });
  }

  Widget _buildFolderWidget(context, text) {
    return BlocBuilder<FileSyncBloc, FileSyncState>(builder: (context, state) {
      return Card(
        child: Column(
          children: [
            Container(
              padding: const EdgeInsets.all(6.0),
              child: Row(children: const [
                Text('DCS Saved Games Location',
                    textAlign: TextAlign.left,
                    style: TextStyle(color: Colors.blueGrey)),
              ]),
            ),
            Row(
              children: [
                ElevatedButton(
                    style: ButtonStyle(
                        padding: MaterialStateProperty.all<EdgeInsets>(
                            const EdgeInsets.all(10))),
                    onPressed: () async {
                      String path = await _openFilePicker();
                      BlocProvider.of<FileSyncBloc>(context)
                          .add(ChangeRootFolder(root: path));
                    },
                    child: Container(
                        padding: const EdgeInsets.all(4.0),
                        child: const Text('Choose Folder'))),
                Card(
                    child: Padding(
                        padding: const EdgeInsets.all(4.0), child: text))
              ],
            )
          ],
        ),
      );
    });
  }

  Future<String> _openFilePicker() async {
    final folder = DirectoryPicker()
      ..title = 'Select the DCS Saved Games folder';
    final result = folder.getDirectory();
    if (result != null) {
      String path = result.path;
      //bool ok = await setLocalFolder(path);
      //super.setState(() {});
      return path;
    }
    return '';
  }
}

class Settings extends StatelessWidget {
  Settings({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 200,
      decoration: BoxDecoration(
        color: Colors.white,
        borderRadius: const BorderRadius.only(
            topLeft: Radius.circular(10),
            topRight: Radius.circular(10),
            bottomLeft: Radius.circular(10),
            bottomRight: Radius.circular(10)),
        boxShadow: [
          BoxShadow(
            color: Colors.grey.withOpacity(0.5),
            spreadRadius: 5,
            blurRadius: 7,
            offset: const Offset(0, 3),
          ),
        ],
      ),
      //padding: const EdgeInsets.all(10.0),
      child: Column(
        children: [
          _title,
          const Divider(),
          const Folder(),
          const Divider(),
          _interfaceSettings()
        ],
      ),
    );
  }

  Widget _interfaceSettings() {
    return BlocBuilder<FileSyncBloc, FileSyncState>(builder: (context, state) {
      return Card(
          child: Column(
        children: [
          const Text("Interface Settings"),
          Row(
            children: [
              Container(
                  padding: const EdgeInsets.fromLTRB(5, 0, 5, 0),
                  child: Row(children: [
                    const Text("Colorblind mode"),
                    Checkbox(
                      //controlAffinity: ListTileControlAffinity.leading,
                      key: state.key,
                      //title: const Text("Colorblind mode"),
                      //secondary: const FaIcon(FontAwesomeIcons.eye),
                      value: state.colorblindMode,
                      onChanged: (value) => context
                          .read<FileSyncBloc>()
                          .add(ChangeColorMode(colorblindMode: value)),
                    ),
                  ])),
              Container(
                  padding: const EdgeInsets.fromLTRB(5, 0, 5, 0),
                  child: ElevatedButton(
                      onPressed: () {
                        resizeWindow(reset: true);
                      },
                      child: const Text("Reset Window Size")))
            ],
          )
        ],
      ));
    });
  }

  Widget _buildColorBlindWidget() {
    return BlocBuilder<FileSyncBloc, FileSyncState>(builder: (context, state) {
      return Card(
        child: Column(
          children: [
            Container(
              padding: const EdgeInsets.all(6.0),
              child: Row(children: const [
                Text('Interface Settings',
                    textAlign: TextAlign.left,
                    style: TextStyle(color: Colors.blueGrey)),
              ]),
            ),
            CheckboxListTile(
              controlAffinity: ListTileControlAffinity.leading,
              key: state.key,
              title: const Text("Colorblind mode"),
              secondary: const FaIcon(FontAwesomeIcons.eye),
              value: state.colorblindMode,
              onChanged: (value) => context
                  .read<FileSyncBloc>()
                  .add(ChangeColorMode(colorblindMode: value)),
            ),
            ElevatedButton(
                onPressed: () {}, child: const Text("Reset Window Size"))
          ],
        ),
      );
    });
  }

  final _title = Container(
      padding: const EdgeInsets.all(6.0),
      decoration:
          BoxDecoration(border: Border.all(width: 1, color: Colors.white10)),
      child: Row(children: const [
        Text('Local Settings',
            textAlign: TextAlign.center, style: TextStyle(fontSize: 15.0)),
        Icon(Icons.settings),
      ]));
}
