import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:x51sync/src/logging/exporter.dart';

IconButton exportButton(context) {
  return IconButton(
      icon: const Icon(CupertinoIcons.graph_square_fill, size: 34.0),
      onPressed: () async {
        String fPath = await LogExporter().toFile();
        final snackBar = SnackBar(content: Text("Log file written to $fPath"));
        ScaffoldMessenger.of(context).showSnackBar(snackBar);
      });
}

FutureBuilder futureBuilder(func, context) {
  return FutureBuilder(
      future: func,
      builder: (context, snapshot) {
        if (snapshot.connectionState == ConnectionState.done) {
          return exportButton(context);
        } else {
          return const CircularProgressIndicator();
        }
      });
}
