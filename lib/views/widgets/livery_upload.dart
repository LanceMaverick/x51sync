import 'package:filepicker_windows/filepicker_windows.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:x51sync/src/livery_upload/bloc/liveryupload_bloc.dart';
import 'package:x51sync/src/livery_upload/bloc/liveryupload_event.dart';
import 'package:x51sync/src/livery_upload/bloc/liveryupload_state.dart';

//TODO convert to single general folder picker class with own bloc
// and inherit all from it (saved games folder picker, mod upload folder etc)

class LivUploadPicker extends StatefulWidget {
  const LivUploadPicker({Key? key}) : super(key: key);

  @override
  LivUploadPickerState createState() => LivUploadPickerState();
}

class LivUploadPickerState extends State<LivUploadPicker> {
  @override
  Widget build(BuildContext context) {
    Text text;
    return BlocBuilder<LiveryUploadBloc, LiveryUploadState>(
        builder: (context, state) {
      if (state.path == '') {
        text = const Text('No folder set', style: TextStyle(color: Colors.red));
      } else {
        text = Text(state.path);
      }
      return _buildFolderWidget(context, text);
    });
  }

  Widget _buildFolderWidget(context, text) {
    return BlocBuilder<LiveryUploadBloc, LiveryUploadState>(
        builder: (context, state) {
      return Card(
        child: Column(
          children: [
            Container(
              padding: const EdgeInsets.all(6.0),
              child: Row(children: const [
                Text('Top folder of livery to upload',
                    textAlign: TextAlign.left,
                    style: TextStyle(color: Colors.blueGrey)),
              ]),
            ),
            Row(
              children: [
                ElevatedButton(
                    style: ButtonStyle(
                        padding: MaterialStateProperty.all<EdgeInsets>(
                            const EdgeInsets.all(10))),
                    onPressed: () async {
                      String path = await _openFilePicker();
                      BlocProvider.of<LiveryUploadBloc>(context)
                          .add(LiveryUploadPathChanged(path: path));
                    },
                    child: Container(
                        padding: const EdgeInsets.all(4.0),
                        child: const Text('Choose Folder'))),
                Card(
                    child: Padding(
                        padding: const EdgeInsets.all(4.0), child: text))
              ],
            )
          ],
        ),
      );
    });
  }

  Future<String> _openFilePicker() async {
    final folder = DirectoryPicker()..title = 'Select the livery';
    final result = folder.getDirectory();
    if (result != null) {
      String path = result.path;
      return path;
    }
    return '';
  }
}

class LivUploadPickerWidget extends StatelessWidget {
  LivUploadPickerWidget({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 150,
      decoration: BoxDecoration(
        color: Colors.white,
        borderRadius: const BorderRadius.only(
            topLeft: Radius.circular(10),
            topRight: Radius.circular(10),
            bottomLeft: Radius.circular(10),
            bottomRight: Radius.circular(10)),
        boxShadow: [
          BoxShadow(
            color: Colors.grey.withOpacity(0.5),
            spreadRadius: 5,
            blurRadius: 7,
            offset: const Offset(0, 3),
          ),
        ],
      ),
      //padding: const EdgeInsets.all(10.0),
      child: Column(
        children: [_title, const Divider(), const LivUploadPicker()],
      ),
    );
  }

  final _title = Container(
      padding: const EdgeInsets.all(6.0),
      decoration:
          BoxDecoration(border: Border.all(width: 1, color: Colors.white10)),
      child: Row(children: const [
        Text('Mod folder',
            textAlign: TextAlign.center, style: TextStyle(fontSize: 15.0)),
        Icon(Icons.settings),
      ]));
}
