import 'package:data_table_2/data_table_2.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:x51sync/models/files.dart';
import 'package:x51sync/src/file_sync/bloc/files_bloc.dart';
import 'package:x51sync/src/file_sync/bloc/files_event.dart';
import 'package:x51sync/src/file_sync/bloc/files_state.dart';
import 'package:x51sync/src/services/x51colors.dart';

// ignore: must_be_immutable
class FileLister extends StatefulWidget {
  dynamic data;
  String title = '';
  FileLister({
    Key? key,
    required this.data,
    required this.title,
  }) : super(key: key);

  @override
  FileListerState createState() => FileListerState();
}

class FileListerState extends State<FileLister> {
  final ScrollController controller = ScrollController();

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<FileSyncBloc, FileSyncState>(builder: (context, state) {
      return Container(child: _listBuilder(context, widget.data));
    });
    //
  }

  DataRow2 _buildRow(context, state, entity) {
    String name = entity.name;
    String localVer = entity.localVersion;
    String remVer = entity.remoteVersion;
    DataCell nameCell = DataCell(Tooltip(
        message: "local: $localVer \nremote: $remVer", child: Text(name)));
    DataCell trackCell = const DataCell(Text('no data'));
    DataCell syncCell = const DataCell(Text('no data'));
    DataCell checkCell = DataCell(_syncCheckBox(context, state, entity));
    bool colorblind = state.colorblindMode;
    if (entity.tracked) {
      trackCell = DataCell(Tooltip(
        message: 'This mod is tracked by ModSync',
        child: Icon(CupertinoIcons.antenna_radiowaves_left_right,
            color: X51Cols().getCol(colorblind, 'ok')),
      ));
      if (entity.downloads.isNotEmpty) {
        syncCell = DataCell(Tooltip(
            message:
                'You either do not have this mod installed, or your local version is out of date',
            child: Icon(CupertinoIcons.xmark_circle_fill,
                color: X51Cols().getCol(colorblind, 'ko'))));
      } else {
        syncCell = DataCell(Tooltip(
            message: 'Your local installation of this mod is up to date',
            child: Icon(CupertinoIcons.checkmark_circle_fill,
                color: X51Cols().getCol(colorblind, 'ok'))));
      }
    } else {
      trackCell = DataCell(Tooltip(
          message: 'This mod is installed locally but is not tracked',
          child: Icon(CupertinoIcons.xmark_circle_fill,
              color: X51Cols().getCol(colorblind, 'neutral'))));
      syncCell = DataCell(Tooltip(
          message:
              'As this mod is not tracked, it will be not be modified by ModSync',
          child: Icon(CupertinoIcons.checkmark_circle_fill,
              color: X51Cols().getCol(colorblind, 'neutral'))));
    }
    return DataRow2(cells: [nameCell, trackCell, syncCell, checkCell]);
  }

  Widget _syncCheckBox(context, state, entity) {
    return BlocBuilder<FileSyncBloc, FileSyncState>(builder: (context, state) {
      String parent = entity.parent ?? '';
      String name = entity.name ?? '';

      if (!entity.tracked || entity.synced) {
        return const Checkbox(
            checkColor: Colors.white, value: true, onChanged: null);
      }

      //hacky way to get the module back from the state using name in the list
      //TODO: couple displayed mod in list to ModuleModel in state

      List<ModuleModel> mods = state.modSyncData![parent]!;
      ModuleModel? foundMod;
      bool include = false;
      for (ModuleModel m in mods) {
        if (m.name == name) {
          foundMod = m;
        }
      }
      include = foundMod!.include;
      return Checkbox(
          checkColor: Colors.white,
          value: include,
          onChanged: (val) => context
              .read<FileSyncBloc>()
              .add(ModFileSyncCheckbox(foundMod!, val ?? false)));
    });
  }

  Widget _listBuilder(context, entities) {
    //TODO: READD
    //return Container();

    return BlocBuilder<FileSyncBloc, FileSyncState>(builder: (context, state) {
      List<DataRow> rows = [];
      for (var entity in entities) {
        rows.add(_buildRow(context, state, entity));
      }
      return DataTable2(columns: const <DataColumn2>[
        DataColumn2(label: Text('Name'), tooltip: "Name of the module"),
        DataColumn2(
            label: Text('Tracked'),
            tooltip:
                "Green if the mod exists in the remote repository and red if it only exists locally. Untracked modules will not be touched by modSync."),
        DataColumn2(
            label: Text('Synced'),
            tooltip:
                "If the module it tracked, this shows how many of its files are up-to-date with the main repository."),
        DataColumn2(
            label: Text('Include'),
            tooltip:
                "If unticked, the module will not be synced, even if it's tracked by modSync."),
      ], rows: rows);
    });
  }
}
