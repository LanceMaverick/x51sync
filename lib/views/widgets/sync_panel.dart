import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:x51sync/src/file_sync/bloc/files_bloc.dart';
import 'package:x51sync/src/file_sync/bloc/files_state.dart';
import 'package:x51sync/src/transfer/transfer_status.dart';

Widget syncPanel(syncState, trackStatus) {
  return BlocBuilder<FileSyncBloc, FileSyncState>(builder: (context, state) {
    if (syncState is TransferFailed) {
      showDialog(
          context: context,
          builder: (context) {
            return AlertDialog(
              title: const Text("Sync Failed"),
              content: Text(syncState.exception.toString()),
            );
          });
    }
    String title = 'Downloading...';

    double totbarVal = syncState.totPercent;
    String totBarLabel =
        '${syncState.sentBytes}/${syncState.totalBytes}: (${syncState.filesSent}/${syncState.totalFiles})';

    //String file = syncState.currFile;
    LinearProgressIndicator totBar = LinearProgressIndicator(
      value: totbarVal,
      valueColor: const AlwaysStoppedAnimation<Color>(Colors.yellow),
      semanticsLabel: totBarLabel,
    );

    List<Widget> fileBars = [];
    for (int i = 0; i < syncState.tracks.length; i++) {
      var track = syncState.tracks[i];
      var trackState = trackStatus[i];
      if (trackState) {
        fileBars.add(const FaIcon(FontAwesomeIcons.check, color: Colors.green));
      } else {
        String fName = track.file;
        double fileBarVal = track.filePercent;
        String fileBarlabel = '${track.fileBytesSent}/${track.fileBytesTot}';
        LinearProgressIndicator fileBar = LinearProgressIndicator(
          value: fileBarVal,
          valueColor: const AlwaysStoppedAnimation<Color>(Colors.yellow),
          semanticsLabel: fileBarlabel,
        );

        Text fNametext = Text(fName,
            style: const TextStyle(color: Colors.blueGrey, fontSize: 12));
        Text fileBarlabelText = Text(fileBarlabel);
        fileBars.addAll([fileBar, fNametext, fileBarlabelText]);
      }
    }

    List<Widget> children = [
      Text(title),
      const Divider(),
      Text(totBarLabel),
      totBar,
      const Divider(),
      const SizedBox(height: 30)
    ];
    children.addAll(fileBars);
    return Container(
        padding: const EdgeInsets.all(100), child: Column(children: children));
  });
}
