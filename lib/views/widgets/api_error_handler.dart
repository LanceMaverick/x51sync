import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:x51sync/src/services/api_state.dart';
import 'package:x51sync/src/session/bloc/session_bloc.dart';

void checkApiErrors(context, state) {
  if (state.msg is ApiAuthExpired) {
    showExpired(context, state.msg);
  } else if (state.msg is ApiTimeout) {
    showTimeout(context, state.msg);
  } else if (state.msg is ApiGenericError) {
    showApiError(context, state.msg);
  }
}

void showExpired(BuildContext context, ApiAuthExpired msg) {
  //context.read<FileSyncBloc>().add(FileSyncInit());
  AlertDialog _dialog = AlertDialog(
    title: const Text('Login Expired'),
    content: const Text('Please log in again to continue'),
    actions: [
      TextButton(
        onPressed: () {
          BlocProvider.of<SessionBloc>(context).add(SessionInit());
          Navigator.pushReplacementNamed(context, '/');
        },
        child: const Text('Login'),
      ),
    ],
  );
  showDialog(
      context: context,
      builder: (BuildContext context) {
        return _dialog;
      });
}

void showTimeout(BuildContext context, ApiTimeout msg) {
  //context.read<FileSyncBloc>().add(FileSyncInit());
  AlertDialog _dialog = AlertDialog(
    title: const Text('Timeout'),
    content: Text(msg.cause.toString()),
    actions: [
      TextButton(
        onPressed: () {
          BlocProvider.of<SessionBloc>(context).add(SessionInit());
          Navigator.pop(context);
        },
        child: const Text('OK'),
      ),
    ],
  );
  showDialog(
      context: context,
      builder: (BuildContext context) {
        return _dialog;
      });
}

void showApiError(BuildContext context, ApiGenericError msg) {
  //context.read<FileSyncBloc>().add(FileSyncInit());
  AlertDialog _dialog = AlertDialog(
    title: const Text('Timeout'),
    content: Text(msg.cause.toString()),
    actions: [
      TextButton(
        onPressed: () {
          BlocProvider.of<SessionBloc>(context).add(SessionInit());
          Navigator.pop(context);
        },
        child: const Text('OK'),
      ),
    ],
  );
  showDialog(
      context: context,
      builder: (BuildContext context) {
        return _dialog;
      });
}
