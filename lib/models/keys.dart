import 'package:crypto_keys/crypto_keys.dart';

abstract class SigKeys {}

class PublicSigKeys extends SigKeys {
  List<KeyPair> keys = [];
}

class PrivateSigKey extends SigKeys {
  KeyPair? key;
  Signer? signer;
}
