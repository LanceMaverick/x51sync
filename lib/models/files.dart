/*Manage the local files */
import 'dart:io';
import 'dart:math';
import 'package:path/path.dart' as p;

class FileData {
  String path;
  DateTime date;
  FileData(this.path, this.date);
}

class FilesData {
  List<FileData> files = [];

  FilesData();

  void add(file) {
    files.add(file);
  }

  FileData? getFile(path) {
    for (FileData f in files) {
      if (f.path == path) {
        return f;
      }
    }
    return null;
  }
}

class FileModelState {
  bool local = false; //file exists locally
  bool remote = false; //file exists remotely
  bool synced = false; //
  bool untracked = true;
  bool download = false;
  bool delete = false;

  bool toDelete() {
    return local && !remote;
  }

  bool toDownload() {
    return remote && !local;
  }
}

class FileModel {
  final ctx = p.Context(style: p.Style.windows);
  late File file;
  late String root = '';
  late String path = '';
  late String rpath = '';
  late DateTime? sysDate;
  late DateTime? repoDate;
  late int? repoSize;
  final FileModelState state = FileModelState();

  FileModel({required this.file, required this.root}) {
    path = file.path.split(root).last;
    rpath = rPath();

    if (file.existsSync()) {
      sysDate = file.lastModifiedSync();
      state.local = true;
      setLocalState();
    }
  }

  String rPath() {
    return path.replaceAll(ctx.separator, '/');
  }

  FileModel.remote({required this.root, required rpath}) {
    path = ctx.join(root, rpath);
    file = File(path);
    setLocalState();
  }

  void setLocalState() {
    if (file.existsSync()) {
      state.local = true;
    }
  }

  void check() {
    if (state.remote == true) {
      state.untracked = false;
      if (state.local == false) {
        state.download = true;
      } else {
        if (sysDate != null && repoDate != null) {
          if (sysDate!.isBefore(repoDate!)) {
            state.download = true;
          }
        }
      }
    }
  }
}

class ModuleModel {
  final String name;
  String parent = '';
  bool tracked = false;
  bool synced = true;
  bool include = true;
  List<FileModel> files = [];
  String localVersion = 'unknown';
  String remoteVersion = 'unknown';

  //TODO .downloads deprecated. Do not need to use download list if not doing hash compare
  // and whole mod is always re-downloaded during sync
  List<FileModel> downloads = [];
  ModuleModel(this.name);

  void setFiles(List<File> allFiles, root) {
    for (var file in allFiles) {
      files.add(FileModel(file: file, root: root));
    }
  }

  void check({bool fileCheck = true}) {
    downloads = [];
    for (FileModel file in files) {
      if (fileCheck) {
        file.check();
      }
      if (file.state.download) {
        downloads.add(file);
      }
    }
    if (downloads.isNotEmpty) {
      synced = false;
    } else {
      synced = true;
    }
  }

  void sync() {
    if (!tracked) {
      return;
    }
  }
}

//placeholder to expand...
class FilesModel {
  final List<File> files = [];
}

class JsonFile {
  final String prefix = 'x51Sync/dcs/';
  String path = '';
  List<String> pathList = [];
  DateTime? date;
  int? size;
  bool isFile = true;
  JsonFile(data) {
    path = data['key'].replaceAll(prefix, '');
    List<String> pl = path.split('/');
    pl.removeWhere((element) => element == '');
    pathList = pl;
    date = DateTime.parse(data['date'] + 'Z');
    size = data['size'];
    isFile = path.substring(path.length - 1) != '/';
  }

  String modData() {
    switch (pathList.length) {
      case 0:
        return 'root';
      case 1:
        return 'mods';
      case 2:
        return 'modType';
      case 3:
        return 'mod';
      case 4:
        return 'modElement';
    }
    return 'modElement';
  }

  String modType() {
    if (modData() == 'mod') {
      return pathList[1];
    }
    return '';
  }

  String modName() {
    String mData = modData();
    if (mData == 'mod' || mData == 'modElement') {
      return pathList[2];
    }
    return '';
  }

  String winPath() {
    String wpath = path.replaceAllMapped(RegExp(r'/'), (match) {
      return r'\';
    });
    return wpath;
  }
}

class UploadFile {
  File file;
  String localRoot = '';

  //late int size;
  late String localPath;
  String? remotePath;
  bool exists = false;

  UploadFile(this.file, this.localRoot) {
    exists = file.existsSync();
    localPath = file.path;
  }

  void setRemotePath(remoteRoot) {
    String relPath =
        //localPath.toLowerCase().split(localRoot.toLowerCase()).last;
        p.relative(localPath, from: localRoot);

    String localRelPath = relPath.replaceAllMapped(RegExp(r'\\'), (match) {
      return r'/';
    });
    remotePath = remoteRoot + '/' + localRelPath;
  }

  Future<int> size() async {
    return await file.length();
  }
}

class UploadFiles {
  String prefix;
  String moduleName;
  List<UploadFile> sigs = [];
  List<UploadFile> files = [];
  //List<Map<String, String>>? downloads;
  UploadFiles(this.prefix, this.moduleName);

  List<Map<String, String>>? downloads(s3Url) {
    List<Map<String, String>> downloads = [];
    for (UploadFile file in files) {
      downloads.add(<String, String>{
        'to': s3Url + file.remotePath,
        'from': file.localPath
      });
    }
    return null;
  }

  void setRemotePaths(prefix) {
    for (UploadFile file in files) {
      file.setRemotePath(prefix);
    }
  }

  int nFiles() {
    return files.length;
  }

  Future<int> size() async {
    int size = 0;
    for (UploadFile file in files) {
      size += await file.size();
    }
    return size;
  }
}

class DownloadFiles {
  List<FileModel> files = [];

  int nFiles() {
    return files.length;
  }

  int size() {
    int size = 0;
    for (FileModel file in files) {
      int fileSize = file.repoSize ?? 0;
      size += fileSize;
    }
    return size;
  }
}

String bytesString(int bytes, {int decimals = 2}) {
  if (bytes <= 0) return "0 B";
  const suffixes = ["B", "KB", "MB", "GB", "TB", "PB", "EB", "ZB", "YB"];
  var i = (log(bytes) / log(1024)).floor();
  return '${(bytes / pow(1024, i)).toStringAsFixed(decimals)} ${suffixes[i]}';
}
