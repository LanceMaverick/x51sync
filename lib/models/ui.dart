import 'package:x51sync/models/files.dart';

abstract class LiveryPopupItem {
  ModuleModel liv;
  LiveryPopupItem(this.liv);
}

class LiveryPopupItemEdit extends LiveryPopupItem {
  LiveryPopupItemEdit(ModuleModel liv) : super(liv);
}

class LiveryPopupItemDelete extends LiveryPopupItem {
  LiveryPopupItemDelete(ModuleModel liv) : super(liv);
}

abstract class AircraftPopupItem {
  String aircraft;
  AircraftPopupItem(this.aircraft);
}

class AircraftPopupItemEdit extends AircraftPopupItem {
  AircraftPopupItemEdit(String aircraft) : super(aircraft);
}

class AircraftPopupItemDelete extends AircraftPopupItem {
  AircraftPopupItemDelete(String aircraft) : super(aircraft);
}
