import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:x51sync/src/services/api_service.dart';

class AuthRepository extends ApiService {
  Future<void> login(username, password, remember) async {
    //await Future.delayed(const Duration(seconds: 3));
    String token = '';
    try {
      token = await doLogin(username, password);
    } catch (e) {
      rethrow;
    }
    if (token == '') {
      throw Exception('failed log in');
    }
    var storage = const FlutterSecureStorage();
    await storage.write(key: "token", value: token);

    String role = await validateToken();
    if (role != 'curator' && role != 'member') {
      throw Exception('User role $role unknown');
    }

    SharedPreferences prefs = await SharedPreferences.getInstance();
    await prefs.setString('role', role);
    await prefs.setBool('remember', remember);
    if (remember) {
      await storage.write(key: "username", value: username);
      await storage.write(key: "password", value: password);
    } else {
      for (var k in {"username", "password"}) {
        String? cred = await storage.read(key: k);
        if (cred != null) {
          await storage.delete(key: k);
        }
      }
    }
  }

  Future<LoginCreds> getStoredCreds() async {
    var storage = const FlutterSecureStorage();
    SharedPreferences prefs = await SharedPreferences.getInstance();

    String username = await storage.read(key: "username") ?? '';
    String password = await storage.read(key: "password") ?? '';
    bool remember = prefs.getBool("remember") ?? false;

    return LoginCreds(username, password, remember);
  }
}

class LoginCreds {
  final String username;
  final String password;
  final bool remember;
  LoginCreds(this.username, this.password, this.remember);
}
