abstract class LoginEvent {}

class LoginUsernameChanged extends LoginEvent {
  final String? username;

  LoginUsernameChanged({this.username});
}

class LoginPasswordChanged extends LoginEvent {
  final String? password;

  LoginPasswordChanged({this.password});
}

class LoginRememberChanged extends LoginEvent {
  final bool? remember;

  LoginRememberChanged({this.remember});
}

class LoginSubmitted extends LoginEvent {}

class LoginSuccess extends LoginEvent {}

class LoginInitial extends LoginEvent {}
