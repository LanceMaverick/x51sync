import 'package:flutter/cupertino.dart';
import 'package:x51sync/auth/form_submission_status.dart';

class LoginState {
  // Validation
  final String username;
  bool get isValidUsername => username.isNotEmpty;
  final String password;
  bool get isValidPassword => password.isNotEmpty;
  final bool remember;
  final FormSubmissionStatus formStatus;
  UniqueKey? key;

  LoginState(
      {this.username = '',
      this.password = '',
      this.remember = false,
      this.key,
      this.formStatus = const InitialFormStatus()});

  LoginState copyWith({
    String? username,
    String? password,
    bool? remember,
    UniqueKey? key,
    FormSubmissionStatus? formStatus,
  }) {
    return LoginState(
        username: username ?? this.username,
        password: password ?? this.password,
        remember: remember ?? this.remember,
        key: key ?? this.key,
        formStatus: formStatus ?? this.formStatus);
  }
}
