// ignore_for_file: must_be_immutable

import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:x51sync/auth/auth_repository.dart';
import 'package:x51sync/auth/form_submission_status.dart';
import 'package:x51sync/auth/login/login_event.dart';
import 'package:x51sync/auth/login/login_state.dart';

import 'package:x51sync/auth/login/login_bloc.dart';
import 'package:x51sync/src/services/globals.dart';
import 'package:x51sync/src/session/bloc/session_bloc.dart';
import 'package:x51sync/views/widgets/log_exporter.dart';

class LoginView extends StatelessWidget {
  // ignore: unused_field
  Timer? _debounce;

  final _formKey = GlobalKey<FormState>();

  final _version = GlobalData.version;

  LoginView({Key? key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    LoginBloc loginBloc = LoginBloc(authRepo: context.read<AuthRepository>());
    return Scaffold(
      appBar: AppBar(actions: [exportButton(context)]),
      body: BlocProvider(
        create: (context) => loginBloc..add(LoginInitial()),
        lazy: false,
        child: _loginForm(context),
      ),
    );
  }

  Widget _loginForm(context) {
    return BlocListener<LoginBloc, LoginState>(
        listener: (context, state) {
          final formStatus = state.formStatus;
          if (formStatus is SubmissionFailed) {
            _showSnackBar(context, formStatus.exception.toString());
          } else if (formStatus is SubmissionSuccess) {
            _successfulLogin(context);
          }
        },
        child: Form(
          key: _formKey,
          child: Padding(
            padding: const EdgeInsets.symmetric(horizontal: 40),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                _logo(),
                _usernameField(),
                _passwordField(),
                rememberField(),
                const SizedBox(height: 20),
                _loginButton(),
                const SizedBox(height: 20),
                const Text(
                  'For any issues, contact Wazzerbosh',
                ),
                const SizedBox(height: 100),
                _infoBar(context),
                // const SizedBox(height: 100),
                //_footer
              ],
            ),
          ),
        ));
  }

  Widget _logo() {
    return Column(
      children: [
        const Image(
          image: AssetImage('assets/images/x51_150.png'),
        ),
        Text(_version,
            style: const TextStyle(fontSize: 10, color: Colors.blueGrey)),
        const SizedBox(height: 30),
        const Text('Login with your hq.x51squadron.com credentials')
      ],
      //padding: const EdgeInsets.all(30.0),
    );
  }

  Widget _usernameField() {
    return BlocBuilder<LoginBloc, LoginState>(builder: (context, state) {
      return TextFormField(
        decoration: const InputDecoration(
          icon: Icon(Icons.person),
          hintText: 'Username',
        ),
        key: state.key,
        initialValue: state.username,
        validator: (value) =>
            state.isValidUsername ? null : 'Username is too short',
        onChanged: (value) {
          context.read<LoginBloc>().add(
                LoginUsernameChanged(username: value),
              );
        },
        onFieldSubmitted: (value) {
          if (_formKey.currentState!.validate()) {
            context.read<LoginBloc>().add(LoginSubmitted());
          }
        },
      );
    });
  }

  Widget _passwordField() {
    return BlocBuilder<LoginBloc, LoginState>(builder: (context, state) {
      return TextFormField(
        obscureText: true,
        decoration: const InputDecoration(
          icon: Icon(Icons.security),
          hintText: 'Password',
        ),
        initialValue: state.password,
        key: state.key,
        validator: (value) =>
            state.isValidPassword ? null : 'Password is too short',
        onChanged: (value) => context.read<LoginBloc>().add(
              LoginPasswordChanged(password: value),
            ),
        onFieldSubmitted: (value) {
          if (_formKey.currentState!.validate()) {
            context.read<LoginBloc>().add(LoginSubmitted());
          }
        },
      );
    });
  }

  Widget rememberField() {
    return BlocBuilder<LoginBloc, LoginState>(builder: (context, state) {
      return SizedBox(
        width: 250,
        child: CheckboxListTile(
          key: state.key,
          title: const Text("Remember me"),
          secondary: const FaIcon(FontAwesomeIcons.userCheck),
          value: state.remember,
          onChanged: (value) => context
              .read<LoginBloc>()
              .add(LoginRememberChanged(remember: value)),
        ),
      );
    });
  }

  Widget _loginButton() {
    return BlocBuilder<LoginBloc, LoginState>(builder: (context, state) {
      SpinKitThreeInOut spinkit = SpinKitThreeInOut(
        color: Theme.of(context).colorScheme.primary,
        size: 40.0,
      );
      return state.formStatus is FormSubmitting
          ? spinkit
          : ElevatedButton(
              onPressed: () {
                if (_formKey.currentState!.validate()) {
                  context.read<LoginBloc>().add(LoginSubmitted());
                }
              },
              child: const Text('Login'),
            );
    });
  }

  Widget _infoBar(context) {
    return Column(children: [
      Row(mainAxisAlignment: MainAxisAlignment.spaceEvenly, children: const [
        Text('By using this software you agree to the disclaimer',
            style: TextStyle(fontSize: 11, color: Colors.grey))
      ]),
      Row(mainAxisAlignment: MainAxisAlignment.spaceEvenly, children: [
        ElevatedButton(
            style: ButtonStyle(
                backgroundColor: MaterialStateProperty.all<Color>(Colors.grey)),
            onPressed: () {
              showDisclaimer(context);
            },
            child: const Text('Disclamer', style: TextStyle(fontSize: 11)))
      ]),
    ]);
  }

  void _successfulLogin(context) {
    //push homepage after successful login
    BlocProvider.of<SessionBloc>(context).add(SessionInit());
    Navigator.pushReplacementNamed(context, '/home');
    /*Navigator.pushAndRemoveUntil<void>(
      context,
      MaterialPageRoute<void>(
          builder: (BuildContext context) => const HomePage()),
      ModalRoute.withName('/'),
    );*/
  }

  void _showSnackBar(BuildContext context, String message) {
    //error message snackbar
    final snackBar = SnackBar(content: Text(message));
    ScaffoldMessenger.of(context).showSnackBar(snackBar);
    context.read<LoginBloc>().add(LoginInitial());
  }
}

showDisclaimer(BuildContext context) {
  Widget okButton = TextButton(
    child: const Text('OK'),
    onPressed: () {
      Navigator.of(context).pop();
    },
  );
  // set up the AlertDialog
  AlertDialog disclaimer =
      AlertDialog(actions: [okButton], content: const Text("""
      This software is provided “as is” without warranty of any kind, 
      either expressed or implied and such software is to be used at your own risk.

      You will be solely responsible for any damage to your computer system or loss 
      of data that results from using this software.
      
      You are solely responsible for adequate protection and backup of the data used 
      in connection with this software
      """));

  showDialog(
    context: context,
    builder: (BuildContext context) {
      return disclaimer;
    },
  );
}
