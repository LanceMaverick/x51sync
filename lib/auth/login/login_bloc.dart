import 'package:f_logs/f_logs.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:x51sync/auth/auth_repository.dart';
import 'package:x51sync/auth/login/login_state.dart';
import 'package:x51sync/auth/login/login_event.dart';
import '../form_submission_status.dart';

class LoginBloc extends Bloc<LoginEvent, LoginState> {
  final AuthRepository authRepo;

  LoginBloc({required this.authRepo}) : super(LoginState()) {
    on<LoginInitial>(_onLoginInitial);
    on<LoginUsernameChanged>(_onLoginUsernameChanged);
    on<LoginPasswordChanged>(_onLoginPasswordChanged);
    on<LoginRememberChanged>(_onLoginRememberChanged);
    on<LoginSubmitted>(_onLoginSubmitted);

    //add(LoginInitial());
  }

  void _onLoginInitial(LoginInitial event, Emitter<LoginState> emit) async {
    try {
      LoginCreds creds = await authRepo.getStoredCreds();
      emit(state.copyWith(
          username: creds.username,
          password: creds.password,
          remember: creds.remember,
          key: UniqueKey(),
          formStatus: const InitialFormStatus()));
    } catch (e) {
      FLog.severe(text: "Failed to get stored credentials: ${e.toString()}");
      rethrow;
    }
  }

  void _onLoginUsernameChanged(
      LoginUsernameChanged event, Emitter<LoginState> emit) {
    emit(state.copyWith(username: event.username));
  }

  void _onLoginPasswordChanged(
      LoginPasswordChanged event, Emitter<LoginState> emit) {
    emit(state.copyWith(password: event.password));
  }

  void _onLoginRememberChanged(
      LoginRememberChanged event, Emitter<LoginState> emit) {
    emit(state.copyWith(remember: event.remember));
  }

  void _onLoginSubmitted(LoginSubmitted event, Emitter<LoginState> emit) async {
    FLog.info(text: "Logging in as ${state.username}");
    emit(state.copyWith(formStatus: FormSubmitting()));
    try {
      await authRepo.login(state.username, state.password, state.remember);
      emit(state.copyWith(formStatus: SubmissionSuccess()));
    } catch (e) {
      FLog.error(
          text: "Logging as ${state.username} failed with ${e.toString()}");
      emit(state.copyWith(formStatus: SubmissionFailed(e)));
    }
  }
}
