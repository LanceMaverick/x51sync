import 'dart:io';

import 'package:f_logs/f_logs.dart';
import 'package:path_provider/path_provider.dart';
import 'package:intl/intl.dart';
import 'package:path/path.dart' as p;

class LogExporter {
  LogExporter();

  Future<String> toFile() async {
    FLog.info(text: "Exporting log file to disk");
    final ctx = p.Context(style: p.Style.windows);
    final directory = await getTemporaryDirectory();
    DateTime now = DateTime.now();
    final dtStr = DateFormat('yyyy-MM-dd–kk-mm-ss').format(now);
    String filePath = ctx.join(directory.path, "X51ModSync-$dtStr.log");
    File file = File(filePath);
    file.writeAsStringSync("[\n", mode: FileMode.append);
    FLog.getAllLogs().then((logs) {
      for (var log in logs) {
        //print(log.toJson());
        file.writeAsStringSync("${log.toJson()},\n", mode: FileMode.append);
        //_storage.writeLogsToFile(log.toJson().toString());
      }
      file.writeAsStringSync("]\n", mode: FileMode.append);
    });
    return file.path;
  }
}
