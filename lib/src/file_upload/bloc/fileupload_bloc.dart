//library fileupload_bloc;
//import 'dart:async';

import 'dart:async';
import 'dart:io';

import 'package:bloc/bloc.dart';
import 'package:f_logs/f_logs.dart';

import 'package:x51sync/models/files.dart';
import 'package:x51sync/src/file_upload/bloc/fileupload_event.dart';
import 'package:x51sync/src/file_upload/bloc/fileupload_state.dart';
import 'package:x51sync/src/services/api_service.dart';
import 'package:x51sync/src/services/api_state.dart';
import 'package:x51sync/src/services/file_service.dart';
import 'package:x51sync/src/services/sync_service.dart';
import 'package:x51sync/src/transfer/transfer_status.dart';

//part 'fileupload_bloc.dart';

class FileuploadBloc extends Bloc<FileuploadEvent, FileuploadState> {
  final FileService fileSvc;
  final ApiService apiSvc;
  final SyncService syncSvc;
  // ignore: unused_field
  StreamSubscription<ApiState>? _uploadSubscription;

  FileuploadBloc(
      {required this.fileSvc, required this.apiSvc, required this.syncSvc})
      : super(FileuploadInitial()) {
    _uploadSubscription =
        apiSvc.uploadStreamController.stream.listen((apiState) {
      if (apiState is ApiTransferState) {
        add(FileuploadApiEvent(apiState));
      } else if (apiState is ApiTransferDone) {
        add(FileuploadModComplete());
      } else if (apiState is ApiTransferFailed) {
        String msg =
            "Error uploading file ${apiState.currFile}: ${apiState.e.toString()}";
        Exception exception = Exception(msg);
        add(FileuploadModFailed(exception));
      }
    });
    on<FileuploadInit>(_onFileuploadInit);
    on<FileuploadModNameChanged>(_onModNameChanged);
    on<FileuploadModTypeChanged>(_onModTypeChanged);
    on<FileuploadPathChanged>(_onPathChanged);
    on<FileuploadNewMod>(_onUploadNewMod);
    on<FileuploadEditMod>(_onUploadEditMod);
    on<FileuploadDeleteMod>(_onDeleteMod);
    on<FileuploadModSubmitted>(_onModSubmitted);
    on<FileuploadApiEvent>(_onFileuploadApiEvent);
    on<FileuploadModComplete>(_onFileuploadModComplete);
    on<FileuploadModFailed>(_onFileuploadModFailed);
  }

  Future<Map<String, List<ModuleModel>>> _getModData(state) async {
    FLog.info(text: "Requesting mod data");
    Map<String, List<ModuleModel>> syncData = {
      'aircraft': [],
      'tech': [],
      'services': [],
    };
    await apiSvc.validateToken();
    for (String modType in fileSvc.modDirs) {
      List<String> mods = await apiSvc.getDCSModNames(modType);
      for (String modName in mods) {
        //List<JsonFile> modFiles = await apiSvc.getDCSMod(modType, modName);
        ModuleModel mod = ModuleModel(modName);
        mod.tracked = true;
        syncData[modType]!.add(mod);
      }
    }
    return syncData;
  }

  void _onFileuploadInit(
      FileuploadInit event, Emitter<FileuploadState> emit) async {
    FLog.info(text: "Initializing FileUploadState");
    String msg = '';
    bool error = false;
    try {
      msg = await apiSvc.validateToken();
      assert(msg == 'curator');
    } catch (e) {
      error = true;
      if (e.toString() == '401') {
        emit(state.copyWith(uploadStatus: TransferAuthFailed()));
      } else if (msg != 'curator') {
        emit(state.copyWith(uploadStatus: TransferNoPrivelages()));
      }
    }
    if (!error) {
      emit(state.copyWith(uploadStatus: GettingModList()));
      try {
        Map<String, List<ModuleModel>> syncData = await _getModData(state);
        emit(state.copyWith(
            selectedMods: syncData[state.modType],
            existingMods: syncData,
            uploadStatus: const InitialTransferStatus()));
      } catch (e) {
        if (e is HttpException && e.message == '401') {
          FLog.error(text: "API Authorization failed on FileUploadInit");
          emit(state.copyWith(uploadStatus: TransferAuthFailed()));
        } else {
          rethrow;
        }
      }
    }
  }

  void _onModNameChanged(
      FileuploadModNameChanged event, Emitter<FileuploadState> emit) {
    emit(state.copyWith(modName: event.modName));
  }

  void _onModTypeChanged(
      FileuploadModTypeChanged event, Emitter<FileuploadState> emit) {
    emit(state.copyWith(
        modType: event.modType,
        selectedMods: state.existingMods![event.modType]));
  }

  void _onPathChanged(
      FileuploadPathChanged event, Emitter<FileuploadState> emit) async {
    FLog.info(text: "File path changed to ${event.path} for mod upload");
    emit(state.copyWith(path: event.path, uploadStatus: ProcessingFiles()));

    List<File> fileList = await fileSvc.getFilesRecursive(event.path);
    List<UploadFile> uploadFileList = [];
    UploadFiles uploadFiles = UploadFiles(state.modType, state.modName);

    for (File file in fileList) {
      uploadFileList.add(UploadFile(file, state.path));
    }
    uploadFiles.files = uploadFileList;
    int bytes = await uploadFiles.size();
    //int bytes = await state.getSize() ?? 0;
    state.sizeStr = bytesString(bytes);
    state.bytes = bytes;
    emit(state.copyWith(
        path: event.path,
        uploadFiles: uploadFiles,
        newPathSubmitted: true,
        ready: true,
        uploadStatus: ProcessingComplete()));
  }

  void _onUploadNewMod(FileuploadNewMod event, Emitter<FileuploadState> emit) {
    FLog.info(text: "Preparing new mod for upload");
    if (event.modType != null) {
      emit(FileuploadState(
          newMod: true,
          path: '',
          ready: false,
          uploadFiles: null,
          modType: event.modType!,
          existingMods: state.existingMods));
    } else {
      emit(FileuploadState(
          newMod: true,
          path: '',
          ready: false,
          uploadFiles: null,
          existingMods: state.existingMods));
    }
  }

  void _onUploadEditMod(
      FileuploadEditMod event, Emitter<FileuploadState> emit) {
    FLog.info(text: "Preparing new version of ${event.mod!.name} for upload");
    emit(state.copyWith(
        newMod: false,
        path: '',
        ready: false,
        mod: event.mod,
        modName: event.mod!.name,
        uploadFiles: null));
  }

  void _onDeleteMod(FileuploadDeleteMod event, Emitter<FileuploadState> emit) {
    FLog.info(text: "Deleting mod: ${event.modType}/${event.mod.name}");
    ModuleModel mod = event.mod;
    String modName = mod.name;
    String modType = event.modType;

    apiSvc.deleteDCSMod(modType, modName);
    emit(state);
  }

  //callback for new module upload submitted
  void _onModSubmitted(
      FileuploadModSubmitted event, Emitter<FileuploadState> emit) async {
    state.uploadFiles!.moduleName = state.modName;
    FLog.info(
        text:
            "Uploading mod ${state.modName} consisting of ${state.uploadFiles!.size()}");
    if (!state.ready) {
      //check state of file checks
      emit(
          state.copyWith(uploadStatus: TransferFailed(Exception('Not ready'))));
    } else if (!state.isValidmodDir) {
      //mod type field validator
      emit(state.copyWith(
          uploadStatus:
              TransferFailed(Exception('Mod Dir ${state.modType} invalid'))));
    } else if (!state.nameCheck()) {
      //mod name field validator
      emit(state.copyWith(
          uploadStatus:
              TransferFailed(Exception('Mod name ${state.modName} invalid'))));
    } else {
      //do upload in async generator stream
      try {
        UploadFiles uploadFiles = state.uploadFiles!;
        uploadFiles.setRemotePaths(
            'x51Sync/dcs/mods/${state.modType}/${state.modName}');
        if (!state.newMod) {
          apiSvc.deleteDCSMod(state.modType, state.modName);
        }
        apiSvc.uploadFiles(uploadFiles, 'mod');
      } catch (e) {
        if (e is HttpException && e.message == '401') {
          FLog.error(text: "API authentication failed: $e");
          emit(state.copyWith(uploadStatus: TransferAuthFailed()));
        } else {
          rethrow;
        }
      }
      emit(state.copyWith(uploadStatus: TransferringFiles(0, 0, '', '', 0, 1)));
      //await subscription.cancel();

    }
  }

  void _onFileuploadApiEvent(
      FileuploadApiEvent event, Emitter<FileuploadState> emit) {
    TransferStatus newUploadState = state.uploadStatus;
    if (newUploadState is TransferringFiles) {
      newUploadState.updateState(event.event);
    }
    emit(state.copyWith(uploadStatus: newUploadState));
  }

  void _onFileuploadModComplete(
      FileuploadModComplete event, Emitter<FileuploadState> emit) {
    emit(state.copyWith(uploadStatus: TransferComplete()));
  }

  void _onFileuploadModFailed(
      FileuploadModFailed event, Emitter<FileuploadState> emit) {
    emit(state.copyWith(uploadStatus: TransferFailed(event.e)));
  }

  @override
  close() {
    _uploadSubscription?.cancel();
    return super.close();
  }
}
