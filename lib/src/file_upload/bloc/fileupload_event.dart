//part of 'fileupload_bloc.dart';

import 'package:x51sync/models/files.dart';
import 'package:x51sync/src/services/api_state.dart';

//class FileuploadEvent extends Equatable {
abstract class FileuploadEvent {
  const FileuploadEvent();

  //@override
  //List<Object> get props => [];
}

class FileuploadInit extends FileuploadEvent {}

class FileuploadModNameChanged extends FileuploadEvent {
  final String? modName;

  const FileuploadModNameChanged({this.modName});
}

class FileuploadModTypeChanged extends FileuploadEvent {
  final String? modType;

  const FileuploadModTypeChanged({this.modType});
}

class FileuploadPathChanged extends FileuploadEvent {
  final String? path;
  const FileuploadPathChanged({this.path});
}

class FileuploadExistingModsChanged extends FileuploadEvent {
  final Map<String, List<ModuleModel>>? existingMods;
  const FileuploadExistingModsChanged({this.existingMods});
}

class FileuploadNewMod extends FileuploadEvent {
  final String? modType;
  const FileuploadNewMod({this.modType});
}

class FileuploadEditMod extends FileuploadEvent {
  final ModuleModel? mod;
  const FileuploadEditMod({this.mod});
}

class FileuploadDeleteMod extends FileuploadEvent {
  final ModuleModel mod;
  final String modType;
  const FileuploadDeleteMod({required this.mod, required this.modType});
}

class FileuploadModSubmitted extends FileuploadEvent {}

class FileuploadApiEvent extends FileuploadEvent {
  final ApiTransferState event;
  FileuploadApiEvent(this.event);
}

class FileuploadModFailed extends FileuploadEvent {
  final Object? e;
  FileuploadModFailed(this.e);
}

class FileuploadModComplete extends FileuploadEvent {}
