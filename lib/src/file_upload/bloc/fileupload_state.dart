//part of 'fileupload_bloc.dart';

//import 'package:equatable/equatable.dart';
import 'package:x51sync/models/files.dart';
import 'package:x51sync/src/transfer/transfer_status.dart';

//TODO: make this more intelligent. Use equatable
//ignore: must_be_immutable
class FileuploadState {
  //fixed DCS mod subdirectories
  final modDirs = ['aircraft', 'tech', 'services'];

  List<String> forbiddenChars = ['<', '>', ':', '/', '\\', '|', '?', '*', '"'];

  //map of mods existing on the server
  Map<String, List<ModuleModel>>? existingMods = {
    'aircraft': <ModuleModel>[],
    'tech': <ModuleModel>[],
    'services': <ModuleModel>[],
  };
  //list of mods existing on the server for the chosen type
  List<ModuleModel> selectedMods = [];

  //state properties from form
  final bool
      newMod; //true if submitting a new mod, false if updating an existing one
  final String modName; //name of the mod
  final String modType; //aircraft, tech or services
  final String path; //local path of the top level folder of the mod

  bool newPathSubmitted = false; //set true when a new path is submitted
  String sizeStr = ""; //cache of mod bytes size as string
  int bytes = 0; //size of mod in bytes
  ModuleModel? mod; //model representation of the mod (no files)
  final UploadFiles? uploadFiles; //list of upload files to send
  bool ready = false; //ready to submit the mod for upload
  final TransferStatus uploadStatus; //status of the uploading

  bool get uniqueModName => nameCheck();
  bool get isValidmodName => modName.isNotEmpty && uniqueModName;
  bool get isValidmodDir => modDirs.contains(modType);

  FileuploadState(
      {this.modName = '',
      this.newMod = true,
      this.mod,
      this.modType = 'aircraft',
      this.path = '',
      this.existingMods,
      this.selectedMods = const [],
      this.newPathSubmitted = false,
      this.sizeStr = '',
      this.bytes = 0,
      this.uploadFiles,
      this.ready = false,
      this.uploadStatus = const InitialTransferStatus()});

  //@override
  //List<Object> get props => [];

  FileuploadState copyWith(
      {Map<String, List<ModuleModel>>? existingMods,
      List<ModuleModel>? selectedMods,
      bool? newMod,
      String? modName,
      String? modType,
      String? path,
      ModuleModel? mod,
      UploadFiles? uploadFiles,
      bool? newPathSubmitted,
      String? sizeStr,
      int? bytes,
      bool? ready,
      TransferStatus? uploadStatus}) {
    return FileuploadState(
        existingMods: existingMods ?? this.existingMods,
        selectedMods: selectedMods ?? this.selectedMods,
        newMod: newMod ?? this.newMod,
        modName: modName ?? this.modName,
        modType: modType ?? this.modType,
        path: path ?? this.path,
        mod: mod ?? this.mod,
        sizeStr: sizeStr ?? this.sizeStr,
        bytes: bytes ?? this.bytes,
        newPathSubmitted: newPathSubmitted ?? this.newPathSubmitted,
        uploadFiles: uploadFiles ?? this.uploadFiles,
        ready: ready ?? this.ready,
        uploadStatus: uploadStatus ?? this.uploadStatus);
  }

  Future<int?> getSize() async {
    if (uploadFiles == null || path == '') {
      return null;
    }
    return await uploadFiles!.size();
  }

  bool nameCheck() {

    if (modName == ''){
      return false;
    }

    for (String char in modName.split('')) {
      if (forbiddenChars.contains(char)) {
        return false;
      }
    }

    if (existingMods == null) {
      throw Exception('unique name check called before mod list populated');
    }
    
    List<ModuleModel> mods = existingMods![modType]!;
    if (newMod) {
      for (var mod in mods) {
        if (mod.name.toLowerCase() == modName.toLowerCase()) {
          return false;
        }
      }
    }
    return true;
  }
}

// ignore: must_be_immutable
class FileuploadInitial extends FileuploadState {}

abstract class ModUpload {
  int totalSize;
  int sizeRemaining;

  ModUpload(
    this.totalSize,
    this.sizeRemaining,
  );
}
