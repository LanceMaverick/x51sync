//part of 'LiveryUpload_bloc.dart';

import 'package:x51sync/models/files.dart';
import 'package:x51sync/src/services/api_state.dart';

//class LiveryUploadEvent extends Equatable {
abstract class LiveryUploadEvent {
  const LiveryUploadEvent();

  //@override
  //List<Object> get props => [];
}

class LiveryUploadInit extends LiveryUploadEvent {}

class LiveryUploadLivNameChanged extends LiveryUploadEvent {
  final String? modName;
  const LiveryUploadLivNameChanged({this.modName});
}

class LiveryUploadAircraftNameChanged extends LiveryUploadEvent {
  final String? aircraft;
  const LiveryUploadAircraftNameChanged({this.aircraft});
}

class LiveryUploadAircraftChanged extends LiveryUploadEvent {
  final String? aircraft;

  const LiveryUploadAircraftChanged({this.aircraft});
}

class LiveryUploadPathChanged extends LiveryUploadEvent {
  final String? path;
  const LiveryUploadPathChanged({this.path});
}

class LiveryUploadExistingLivsChanged extends LiveryUploadEvent {
  final Map<String, List<ModuleModel>>? existingMods;
  const LiveryUploadExistingLivsChanged({this.existingMods});
}

class LiveryUploadNewLiv extends LiveryUploadEvent {}

class LiveryUploadEditLiv extends LiveryUploadEvent {
  final ModuleModel mod;
  const LiveryUploadEditLiv({required this.mod});
}

class LiveryUploadDeleteLiv extends LiveryUploadEvent {
  final ModuleModel mod;
  const LiveryUploadDeleteLiv({required this.mod});
}

class LiveryUploadEditAircraft extends LiveryUploadEvent {
  final String aircraft;
  const LiveryUploadEditAircraft({required this.aircraft});
}

class LiveryUploadDeleteAircraft extends LiveryUploadEvent {
  final String aircraft;
  const LiveryUploadDeleteAircraft({required this.aircraft});
}

class LiveryUploadLivSubmitted extends LiveryUploadEvent {}

class LiveryUploadApiEvent extends LiveryUploadEvent {
  final ApiTransferState event;
  LiveryUploadApiEvent(this.event);
}

class LiveryUploadFailed extends LiveryUploadEvent {
  final Object? e;
  LiveryUploadFailed(this.e);
}

class LiveryUploadComplete extends LiveryUploadEvent {}

class LiveryUploadReset extends LiveryUploadEvent {}
