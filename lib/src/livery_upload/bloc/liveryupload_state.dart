//part of 'LiveryUpload_bloc.dart';

//import 'package:equatable/equatable.dart';
import 'package:x51sync/models/files.dart';
import 'package:x51sync/src/transfer/transfer_status.dart';

//TODO: make this more intelligent. Use equatable
//ignore: must_be_immutable
class LiveryUploadState {
  List<String> forbiddenChars = ['<', '>', ':', '/', '\\', '|', '?', '*', '"'];
  //fixed DCS mod subdirectories
  List<String> modDirs = [];

  //map of mods existing aircraft liveries
  Map<String, List<ModuleModel>>? existingMods = {};
  //list of mods existing on the server for the chosen type
  List<ModuleModel> selectedMods = [];

  //state properties from form
  final bool
      newMod; //true if submitting a new mod, false if updating an existing one
  final String modName; //name of the mod
  final String aircraft; //aircraft, tech or services
  bool isBatch = false;
  final String path; //local path of the top level folder of the mod

  bool newPathSubmitted = false; //set true when a new path is submitted
  String sizeStr = ""; //cache of mod bytes size as string
  int bytes = 0; //size of mod in bytes
  ModuleModel? mod; //model representation of the mod (no files)
  final UploadFiles? uploadFiles; //list of upload files to send
  bool ready = false; //ready to submit the mod for upload
  final TransferStatus uploadStatus; //status of the uploading

  bool get uniqueModName => nameCheck();
  bool get isValidmodName => modName.isNotEmpty && uniqueModName;
  bool get isValidmodDir => modDirs.contains(aircraft);

  LiveryUploadState(
      {this.modName = '',
      this.newMod = true,
      this.mod,
      this.aircraft = '',
      this.isBatch = false,
      this.path = '',
      this.existingMods,
      this.selectedMods = const [],
      this.newPathSubmitted = false,
      this.sizeStr = '',
      this.bytes = 0,
      this.uploadFiles,
      this.ready = false,
      this.uploadStatus = const InitialTransferStatus()});

  //@override
  //List<Object> get props => [];

  LiveryUploadState copyWith(
      {Map<String, List<ModuleModel>>? existingMods,
      List<ModuleModel>? selectedMods,
      bool? newMod,
      String? modName,
      String? aircraft,
      bool? isBatch,
      String? path,
      ModuleModel? mod,
      UploadFiles? uploadFiles,
      bool? newPathSubmitted,
      String? sizeStr,
      int? bytes,
      bool? ready,
      TransferStatus? uploadStatus}) {
    return LiveryUploadState(
        existingMods: existingMods ?? this.existingMods,
        selectedMods: selectedMods ?? this.selectedMods,
        newMod: newMod ?? this.newMod,
        modName: modName ?? this.modName,
        aircraft: aircraft ?? this.aircraft,
        isBatch: isBatch ?? this.isBatch,
        path: path ?? this.path,
        mod: mod ?? this.mod,
        sizeStr: sizeStr ?? this.sizeStr,
        bytes: bytes ?? this.bytes,
        newPathSubmitted: newPathSubmitted ?? this.newPathSubmitted,
        uploadFiles: uploadFiles ?? this.uploadFiles,
        ready: ready ?? this.ready,
        uploadStatus: uploadStatus ?? this.uploadStatus);
  }

  Future<int?> getSize() async {
    if (uploadFiles == null || path == '') {
      return null;
    }
    return await uploadFiles!.size();
  }

  bool nameCheck() {
    if (!newMod) {
      return true;
    }
    if (existingMods == null) {
      throw Exception('unique name check called before mod list populated');
    }
    List<ModuleModel> mods = existingMods![aircraft]!;

    for (String char in modName.split('')) {
      if (forbiddenChars.contains(char)) {
        return false;
      }
    }

    for (var mod in mods) {
      if (mod.name.toLowerCase() == modName.toLowerCase()) {
        return false;
      }
    }
    return true;
  }

  bool parentCheck() {
    if (aircraft == '') {
      return false;
    }
    for (String char in aircraft.split('')) {
      if (forbiddenChars.contains(char)) {
        return false;
      }
    }
    return true;
  }

  String? parentFieldCheck() {
    if (aircraft == '') {
      return null;
    }
    for (String char in aircraft.split('')) {
      if (forbiddenChars.contains(char)) {
        return 'Name contains forbidden character "$char"';
      }
    }
    return null;
  }
}

// ignore: must_be_immutable
/*
class LiveryUploadInitial extends LiveryUploadState {}

abstract class ModUpload {
  int totalSize;
  int sizeRemaining;

  ModUpload(
    this.totalSize,
    this.sizeRemaining,
  );
}
*/