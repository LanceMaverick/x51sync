//library LiveryUpload_bloc;
//import 'dart:async';

import 'dart:async';
import 'dart:io';

import 'package:bloc/bloc.dart';
import 'package:f_logs/model/flog/flog.dart';
import 'package:x51sync/models/files.dart';
import 'package:x51sync/src/livery_upload/bloc/liveryupload_event.dart';
import 'package:x51sync/src/livery_upload/bloc/liveryupload_state.dart';
import 'package:x51sync/src/transfer/transfer_status.dart';
import 'package:x51sync/src/services/api_state.dart';
import 'package:x51sync/src/services/api_service.dart';
import 'package:x51sync/src/services/file_service.dart';
import 'package:x51sync/src/services/sync_service.dart';

//part 'LiveryUpload_bloc.dart';

class LiveryUploadBloc extends Bloc<LiveryUploadEvent, LiveryUploadState> {
  final FileService fileSvc;
  final ApiService apiSvc;
  final SyncService syncSvc;
  // ignore: unused_field
  StreamSubscription<ApiState>? _uploadSubscription;

  LiveryUploadBloc(
      {required this.fileSvc, required this.apiSvc, required this.syncSvc})
      : super(LiveryUploadState()) {
    _uploadSubscription =
        apiSvc.uploadStreamController.stream.listen((apiState) {
      if (apiState is ApiTransferState) {
        add(LiveryUploadApiEvent(apiState));
      } else if (apiState is ApiTransferDone) {
        add(LiveryUploadComplete());
      } else if (apiState is ApiTransferFailed) {
        String msg =
            "Error uploading file ${apiState.currFile}: ${apiState.e.toString()}";
        Exception exception = Exception(msg);
        add(LiveryUploadFailed(exception));
      }
    });
    on<LiveryUploadInit>(_onLiveryUploadInit);
    on<LiveryUploadLivNameChanged>(_onLivNameChanged);
    on<LiveryUploadAircraftChanged>(_onAircraftChanged);
    on<LiveryUploadPathChanged>(_onPathChanged);
    on<LiveryUploadNewLiv>(_onUploadNewLiv);

    on<LiveryUploadEditLiv>(_onEditLiv);
    on<LiveryUploadDeleteLiv>(_onDeleteLiv);

    on<LiveryUploadAircraftNameChanged>(_onAircraftNameChanged);
    on<LiveryUploadEditAircraft>(_onEditAircraft);
    on<LiveryUploadDeleteAircraft>(_onDeleteAircraft);

    on<LiveryUploadLivSubmitted>(_onLivSubmitted);
    on<LiveryUploadApiEvent>(_onLiveryUploadApiEvent);
    on<LiveryUploadComplete>(_onLiveryUploadComplete);
    on<LiveryUploadFailed>(_onLiveryUploadFailed);

    on<LiveryUploadReset>(_onLiveryUploadReset);
  }

  void _onLiveryUploadInit(
      LiveryUploadInit event, Emitter<LiveryUploadState> emit) async {
    FLog.info(text: "Initializing LiveryUploadState");
    String msg = '';
    bool error = false;
    try {
      msg = await apiSvc.validateToken();
      assert(msg == 'curator');
    } catch (e) {
      error = true;
      if (e.toString() == '401') {
        FLog.error(text: "Authentication error: $e");
        emit(state.copyWith(uploadStatus: TransferAuthFailed()));
      } else if (msg != 'curator') {
        FLog.error(text: "User does not have required privelages: $msg");
        emit(state.copyWith(uploadStatus: TransferNoPrivelages()));
      }
    }
    if (!error) {
      emit(state.copyWith(
          //selectedMods: syncData[state.modType],
          uploadStatus: const InitialTransferStatus()));
    }
  }

  void _onLivNameChanged(
      LiveryUploadLivNameChanged event, Emitter<LiveryUploadState> emit) {
    emit(state.copyWith(modName: event.modName));
  }

  void _onAircraftChanged(
      LiveryUploadAircraftChanged event, Emitter<LiveryUploadState> emit) {
    emit(state.copyWith(
        aircraft: event.aircraft,
        selectedMods: state.existingMods![event.aircraft]));
  }

  void _onPathChanged(
      LiveryUploadPathChanged event, Emitter<LiveryUploadState> emit) async {
    FLog.info(text: "Setting files path for livery upload as: ${event.path}");
    emit(state.copyWith(path: event.path, uploadStatus: ProcessingFiles()));

    List<File> fileList = await fileSvc.getFilesRecursive(event.path);
    List<UploadFile> uploadFileList = [];
    UploadFiles uploadFiles = UploadFiles(state.aircraft, state.modName);

    for (File file in fileList) {
      uploadFileList.add(UploadFile(file, state.path));
    }

    uploadFiles.files = uploadFileList;
    int bytes = await uploadFiles.size();
    //int bytes = await state.getSize() ?? 0;
    state.sizeStr = bytesString(bytes);
    state.bytes = bytes;
    emit(state.copyWith(
        path: event.path,
        uploadFiles: uploadFiles,
        newPathSubmitted: true,
        ready: true,
        uploadStatus: ProcessingComplete()));
  }

  void _onUploadNewLiv(
      LiveryUploadNewLiv event, Emitter<LiveryUploadState> emit) {
    FLog.info(text: "Preparing upload of new livery");
    emit(state.copyWith(
        newMod: true, path: '', ready: false, uploadFiles: null));
  }

  void _onAircraftNameChanged(
      LiveryUploadAircraftNameChanged event, Emitter<LiveryUploadState> emit) {
    emit(state.copyWith(aircraft: event.aircraft));
  }

  void _onEditLiv(LiveryUploadEditLiv event, Emitter<LiveryUploadState> emit) {
    FLog.info(
        text:
            "Preparing edit of livery: ${event.mod.parent}/${event.mod.name}");
    emit(
      state.copyWith(
          newMod: false,
          path: '',
          ready: false,
          mod: event.mod,
          modName: event.mod.name,
          aircraft: event.mod.parent,
          uploadFiles: null,
          uploadStatus: SubmissionRequested()),
    );
  }

  void _onDeleteLiv(
      LiveryUploadDeleteLiv event, Emitter<LiveryUploadState> emit) async {
    ModuleModel mod = event.mod;
    String livery = mod.name;
    String aircraft = mod.parent;
    FLog.info(text: "Deleting livery: $aircraft/$livery");
    await apiSvc.deleteDCSLivery(aircraft, livery);
    //TODO: error handling
    emit(state);
  }

  void _onEditAircraft(
      LiveryUploadEditAircraft event, Emitter<LiveryUploadState> emit) {
    FLog.info(text: "Editing liveries for: ${event.aircraft}");

    emit(
      state.copyWith(
          newMod: false,
          path: '',
          ready: false,
          isBatch: true,
          aircraft: event.aircraft,
          uploadFiles: null,
          uploadStatus: SubmissionRequested()),
    );
  }

  void _onDeleteAircraft(
      LiveryUploadDeleteAircraft event, Emitter<LiveryUploadState> emit) async {
    String aircraft = event.aircraft;
    FLog.info(text: "Deleting all liveries for: ${event.aircraft}");
    await apiSvc.deleteDCSLiveryAircraft(aircraft);
    //TODO: error handling
    emit(state);
  }

  //callback for new module upload submitted
  void _onLivSubmitted(
      LiveryUploadLivSubmitted event, Emitter<LiveryUploadState> emit) async {
    state.uploadFiles!.moduleName = state.modName;
    FLog.info(text: "Submitting new livery");
    if (!state.ready) {
      //check state of file checks
      emit(
          state.copyWith(uploadStatus: TransferFailed(Exception('Not ready'))));
    } else if (state.isBatch && !state.parentCheck()) {
      emit(state.copyWith(
          uploadStatus: TransferFailed(
              Exception('Aircraft name ${state.aircraft} invalid'))));
    } else if (!state.nameCheck()) {
      //mod name field validator
      emit(state.copyWith(
          uploadStatus: TransferFailed(
              Exception('Livery name ${state.modName} invalid'))));
    } else {
      //do upload in async generator stream
      try {
        UploadFiles uploadFiles = state.uploadFiles!;
        String pathStr = 'x51Sync/dcs/liveries/${state.aircraft}';

        if (!state.isBatch) {
          pathStr = '$pathStr/${state.modName}';
        }

        uploadFiles.setRemotePaths(pathStr);

        apiSvc.uploadFiles(state.uploadFiles!, 'liv');
      } catch (e) {
        if (e is HttpException && e.message == '401') {
          FLog.error(text: "API authentication error: $e");
          emit(state.copyWith(uploadStatus: TransferAuthFailed()));
        } else {
          rethrow;
        }
      }
      emit(state.copyWith(uploadStatus: TransferringFiles(0, 0, '', '', 0, 1)));
      //await subscription.cancel();

    }
  }

  void _onLiveryUploadReset(
      LiveryUploadReset event, Emitter<LiveryUploadState> emit) {}

  void _onLiveryUploadApiEvent(
      LiveryUploadApiEvent event, Emitter<LiveryUploadState> emit) {
    TransferStatus newUploadState = state.uploadStatus;
    if (newUploadState is TransferringFiles) {
      newUploadState.updateState(event.event);
    }
    emit(state.copyWith(uploadStatus: newUploadState));
  }

  void _onLiveryUploadComplete(
      LiveryUploadComplete event, Emitter<LiveryUploadState> emit) {
    emit(state.copyWith(uploadStatus: TransferComplete()));
  }

  void _onLiveryUploadFailed(
      LiveryUploadFailed event, Emitter<LiveryUploadState> emit) {
    emit(LiveryUploadState());
  }

  @override
  close() {
    _uploadSubscription?.cancel();
    return super.close();
  }
}
