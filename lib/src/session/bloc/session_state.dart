part of 'session_bloc.dart';

class SessionState {
  dynamic msg;
  bool inError = false;
  SessionState(this.msg, {this.inError = false});

  SessionState copyWith(dynamic msg, {bool? inError}) {
    return SessionState(msg ?? this.msg, inError: inError ?? this.inError);
  }
}
