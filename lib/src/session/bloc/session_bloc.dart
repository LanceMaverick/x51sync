import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:x51sync/src/services/api_service.dart';
import 'package:x51sync/src/services/api_state.dart';

part 'session_event.dart';
part 'session_state.dart';

class SessionBloc extends Bloc<SessionEvent, SessionState> {
  final ApiService apiSvc;

  // ignore: unused_field
  StreamSubscription<Exception>? _errorSubscription;

  SessionBloc({required this.apiSvc}) : super(SessionState(null)) {
    _errorSubscription = apiSvc.errorStreamController.stream.listen((error) {
      //splitting errors between bloc events for future
      //elaboration or error types, but currently they
      //all do the same thing...
      if (error is ApiAuthExpired) {
        add(SessionExpired(error));
      } else if (error is ApiTimeout) {
        add(SessionApiTimeout(error));
      } else if (error is ApiGenericError) {
        add(SessionGenericApiError(error));
      }
    });

    on<SessionInit>(_onInit);
    on<SessionExpired>(_onExpired);
    on<SessionApiTimeout>(_onApiTimeout);
    on<SessionGenericApiError>(_onGenericApiError);
  }

  void _onInit(SessionInit event, Emitter<SessionState> emit) {
    emit(SessionState(null));
  }

  void _onExpired(SessionExpired event, Emitter<SessionState> emit) {
    emit(state.copyWith(event.error, inError: true));
  }

  void _onApiTimeout(
      SessionApiTimeout event, Emitter<SessionState> emit) async {
    emit(state.copyWith(event.error));
  }

  void _onGenericApiError(
      SessionGenericApiError event, Emitter<SessionState> emit) {
    emit(state.copyWith(event.error));
  }
}
