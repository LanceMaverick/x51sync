part of 'session_bloc.dart';

abstract class SessionEvent {
  const SessionEvent();
}

class SessionInit extends SessionEvent {}

class SessionExpired extends SessionEvent {
  ApiAuthExpired error;
  SessionExpired(this.error);
}

class SessionApiTimeout extends SessionEvent {
  ApiTimeout error;
  SessionApiTimeout(this.error);
}

class SessionGenericApiError extends SessionEvent {
  Exception error;
  SessionGenericApiError(this.error);
}

