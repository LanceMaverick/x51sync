import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';

class X51Cols {
  static const Color okTrue = Colors.green;
  static const Color okCB = Color.fromRGBO(12, 123, 220, 1);

  static const Color koTrue = Colors.red;
  static const Color koCB = Color.fromRGBO(255, 194, 10, 1);

  static const Color neutTrue = Colors.blueGrey;
  static const Color neutCB = Colors.black;

  Future<bool> getMode() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    bool mode = prefs.getBool('colorblindMode') ?? false;
    return mode;
  }

  Color getCol(mode, type) {
    //bool mode = await getMode();
    if (mode) {
      switch (type) {
        case "ok":
          {
            return okCB;
          }
        case "ko":
          {
            return koCB;
          }
        case "neutral":
          {
            return neutCB;
          }
      }
    } else {
      switch (type) {
        case "ok":
          {
            return okTrue;
          }
        case "ko":
          {
            return koTrue;
          }
        case "neutral":
          {
            return neutTrue;
          }
      }
    }
    return neutTrue;
  }
}
