import 'package:f_logs/f_logs.dart';
import 'package:x51sync/models/files.dart';
import 'dart:io';
import 'package:path/path.dart' as p;
import 'package:x51sync/src/services/api_service.dart';

class SyncService {
  //Map<String, List<ModuleModel>> modData(FilesData local, FilesData remote) {}
  final ctx = p.Context(style: p.Style.windows);
  //Map<String, List<ModuleModel>>? mods;
  //List<ModuleModel>? liveries;

  Future<ModuleModel> modFileData(
      ModuleModel mod, List<JsonFile> modFiles, String root) async {
    bool versionFound = false;
    for (JsonFile file in modFiles) {
      String relPath = file.winPath();
      String absPath = ctx.join(root, relPath);
      File locFile = File(absPath);
      FileModel mFile = FileModel(root: root, file: locFile);

      if (!versionFound && locFile.uri.pathSegments.last == 'entry.lua') {
        try {
          mod.localVersion = getModVersion(locFile.readAsLinesSync());
        } catch (e) {
          mod.localVersion = 'unknown';
        }
        List<String> remoteEntry = await ApiService().readRemoteFile(mFile);
        mod.remoteVersion = getModVersion(remoteEntry);
        versionFound = true;
      }
      mFile.state.remote = true;
      mFile.repoDate = file.date;
      mFile.repoSize = file.size;
      if (file.isFile) {
        mod.files.add(mFile);
      }
    }

    mod.check();

    return mod;
  }

  String getModVersion(List<String> lines) {
    try {
      for (String line in lines) {
        if (line.contains('version')) {
          RegExp re = RegExp(r'"(.*)",');
          final match = re.firstMatch(line);
          if (match != null) {
            String version = match.group(0) ?? 'unknown';
            version = version.replaceAll('"', '').replaceAll(',', '');
            return version;
          }
        }
      }
    } catch (e) {
      FLog.error(text: 'Failed to find mod version with error: $e');
      return 'unknown';
    }
    return 'unknown';
  }

  List<ModuleModel> remoteFilesToMods(root, remote, mType, modsOnly) {
    List<ModuleModel> mods = <ModuleModel>[];

    //first pass: add mods.
    for (JsonFile file in remote) {
      if (file.modData() != 'mod') {
        continue;
      }
      String name = file.modName();
      // top folder is mod type, only 1 layer deep and ends in a
      if (file.modType() == mType && file.isFile == false) {
        ModuleModel mod = ModuleModel(name);
        mod.tracked = true;
        mods.add(mod);
      }
    }
    if (modsOnly) {
      return mods;
    }

    //second pass: add mod files
    for (ModuleModel mod in mods) {
      for (JsonFile file in remote) {
        if (file.modData() != 'modElement') {
          continue;
        }
        String name = file.modName();
        if (name == mod.name && file.isFile) {
          String relPath = file.winPath();
          String absPath = ctx.join(root, relPath);
          File locFile = File(absPath);
          FileModel mFile = FileModel(root: root, file: locFile);
          mFile.state.remote = true;
          mFile.repoDate = file.date;
          mFile.repoSize = file.size;

          mod.files.add(mFile);
        }
        mod.check();
      }
    }
    return mods;
  }

  List<String> getModDownloadList(mods, {fileCheck = false}) {
    List<String> downloads = [];
    for (var k in mods.keys) {
      for (ModuleModel mod in mods[k]) {
        if (fileCheck) {
          mod.check();
        }
        for (FileModel file in mod.downloads) {
          downloads.add(file.file.path);
        }
      }
    }
    return downloads;
  }

  Map<String, List<ModuleModel>> getModData(root, remote, {modsOnly = false}) {
    Map<String, List<ModuleModel>> modData = {};

    for (String mType in ['aircraft', 'tech', 'services']) {
      modData[mType] = remoteFilesToMods(root, remote, mType, modsOnly);
    }
    return modData;
  }

  FilesData filterType(FilesData files, String mType) {
    FilesData filteredFiles = FilesData();
    for (var file in files.files) {
      if (file.path.split(ctx.separator)[0].toLowerCase() == mType) {
        filteredFiles.add(file);
      }
    }
    return filteredFiles;
  }
}
