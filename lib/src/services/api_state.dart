abstract class ApiState {}

class ApiTransferState extends ApiState {
  final String context;
  final int totFiles;
  final int filesRemaining;
  final String currFile;
  final int totBytes;
  final int sentBytes; //either sent to or from server
  final int currFileTotBytes;
  final int currFileSentBytes;
  List<String> errors = [];
  late double filePercent; //0-1
  late double totPercent; //0-1
  int track;

  void _calcPercents() {
    totPercent = totBytes != 0 ? sentBytes / totBytes : 0;
    filePercent =
        currFileTotBytes != 0 ? currFileSentBytes / currFileTotBytes : 0;
  }

  void addError(e) {
    errors.add(e);
  }

  ApiTransferState(
      {this.context = '',
      this.totFiles = 0,
      this.filesRemaining = 0,
      this.currFile = '',
      this.totBytes = 0,
      this.sentBytes = 0,
      this.currFileTotBytes = 0,
      this.currFileSentBytes = 0,
      this.errors = const [],
      this.track = 0}) {
    _calcPercents();
  }

  ApiTransferState copyWith(
      {String? context,
      int? totFiles,
      int? filesRemaining,
      String? currFile,
      int? totBytes,
      int? sentBytes,
      int? currFileTotBytes,
      int? currFileSentBytes,
      int? track = 0}) {
    _calcPercents();
    return ApiTransferState(
        context: context ?? this.context,
        totFiles: totFiles ?? this.totFiles,
        filesRemaining: filesRemaining ?? this.filesRemaining,
        currFile: currFile ?? this.currFile,
        totBytes: totBytes ?? this.totBytes,
        sentBytes: sentBytes ?? this.sentBytes,
        currFileTotBytes: currFileTotBytes ?? this.currFileTotBytes,
        currFileSentBytes: currFileSentBytes ?? this.currFileSentBytes,
        errors: errors,
        track: track ?? this.track);
  }
}

class ApiTransferDone extends ApiState {
  final String context;
  final int track;
  ApiTransferDone(this.context, this.track);
}

class ApiTransferFailed extends ApiState {
  final String context;
  final String currFile;
  final Exception e;
  ApiTransferFailed(this.context, this.currFile, this.e);
}

abstract class ApiException implements Exception {}

class ApiAuthExpired implements ApiException {
  String cause;
  ApiAuthExpired(this.cause);
}

class ApiTimeout implements ApiException {
  String cause;
  ApiTimeout(this.cause);
}

class ApiGenericError implements ApiException {
  String cause;
  ApiGenericError(this.cause);
}
