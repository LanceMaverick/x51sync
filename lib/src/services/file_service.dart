/*Manage the local files */
import 'dart:io';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:path/path.dart' as p;
import 'package:pointycastle/export.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:x51sync/models/files.dart';
import 'package:x51sync/models/keys.dart';

//this is all very disgusting, but will improve when
// importing it to blocs
class FileService {
  final List<String> modDirs = ['tech', 'aircraft', 'services'];
  final List<String> livDirs = [];
  final ctx = p.Context(style: p.Style.windows);
  final digester = SHA256Digest();

  PrivateSigKey privKey = PrivateSigKey();
  PublicSigKeys pubKeys = PublicSigKeys();

  Future<List<Directory>> getFolders(path) async {
    List<Directory> folders = [];
    Directory dir = Directory(path);
    if (await dir.exists()) {
      await for (var entity in dir.list(recursive: false, followLinks: true)) {
        if (entity is Directory) {
          folders.add(entity);
        }
      }
    }
    return folders;
  }

  Future<List<File>> getFilesRecursive(path) async {
    List<File> files = [];
    Directory dir = Directory(path);
    await for (var entity in dir.list(recursive: true, followLinks: true)) {
      if (entity is File) {
        files.add(entity);
      }
    }
    return files;
  }

  Future<Map<String, List<File>>> getLocalTree(path) async {
    Map<String, List<File>> tree = {'mods': [], 'liveries': []};
    for (var folder in ['mods', 'liveries']) {
      tree[folder] = await getFilesRecursive(ctx.join(path, folder));
    }
    return tree;
  }

  Future<List<File>> getLocalModsTree(root) async {
    String path = ctx.join(root, 'mods');
    return await getFilesRecursive(path);
  }

  Future<ModuleModel> getMod(path, root) async {
    ModuleModel module = ModuleModel(path.split(ctx.separator).last);
    List<File> files = await getFilesRecursive(path);
    module.setFiles(files, root);
    return module;
  }

  Future<Map<String, List<ModuleModel>>> getMods(root) async {
    String path = ctx.join(root, 'mods');

    Map<String, List<ModuleModel>> modFiles = {};
    for (var mds in modDirs) {
      modFiles[mds] = <ModuleModel>[];
      String mPath = ctx.join(path, mds);
      List<Directory> modList = await getFolders(mPath);
      for (var md in modList) {
        modFiles[mds]!.add(await getMod(md, root));
      }
      //List<FileSystemEntity> modList = await dir.list().toList();
    }
    return modFiles;
  }

  Future<List<FileSystemEntity>> testGetMods() async {
    String home = Platform.environment['USERPROFILE'] as String;
    var context = p.Context(style: p.Style.windows);
    String path = context.join(home, 'Saved Games', 'DCS.openbeta', 'mods');
    Directory dir = Directory(path); //placeholder
    List<FileSystemEntity> modList = await dir.list().toList();
    return modList;
  }

  Future<bool> folderExists(path) async {
    if (path != '') {
      return await Directory(path).exists();
    } else {
      return false;
    }
  }

  Future<String> localFolder() async {
    var storage = const FlutterSecureStorage();
    String folder = await storage.read(key: 'localFolder') ?? '';

    return folder;
  }

  Future<bool> setLocalFolder(path) async {
    bool exists = await folderExists(path);
    if (!exists) {
      return false;
    }
    var storage = const FlutterSecureStorage();
    await storage.write(key: 'localFolder', value: path);
    return true;
  }

  Future<Map<String, List<Directory>>> getFileMap(path) async {
    Map<String, List<Directory>> mods = {};

    String rootPath = await localFolder();
    if (rootPath == '') {
      throw Exception('DCS Saved Games folder is not set.');
    }
    for (String modDir in modDirs) {
      path = ctx.join(rootPath, modDir);
      Directory dir = Directory(path);
      mods[modDir] = [
        await for (var entity in dir.list())
          if (entity is Directory) entity
      ];
    }
    return mods; //un-assigned and must be future
  }

  void saveModPaths(mods) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    for (var key in mods.keys) {
      await prefs.setInt(key, mods[key]);
    }
  }

  Future<Map<String, List<ModuleModel>>> addUntrackedMods(
      String root, Map<String, List<ModuleModel>> syncData) async {
    for (String key in syncData.keys) {
      String rPath = ctx.join(root, 'mods', key);
      List<Directory> modsDirs = await getFolders(rPath);
      for (Directory dir in modsDirs) {
        String name = dir.path.split(ctx.separator).last;
        bool tracked = false;
        for (ModuleModel mod in syncData[key]!) {
          if (mod.name == name) {
            tracked = true;
          }
        }
        if (!tracked) {
          ModuleModel localMod = ModuleModel(name);
          localMod.tracked = false;
          syncData[key]!.add(localMod);
        }
      } //dir loop
    } // key loop
    return syncData;
  }

  Future<Map<String, List<ModuleModel>>> addUntrackedLiveries(
      String root, Map<String, List<ModuleModel>> syncData) async {
    for (String key in syncData.keys) {
      String rPath = ctx.join(root, 'liveries', key);
      List<Directory> livDirs = await getFolders(rPath);
      for (Directory dir in livDirs) {
        String name = dir.path.split(ctx.separator).last;
        bool tracked = false;
        for (ModuleModel liv in syncData[key]!) {
          if (liv.name == name) {
            tracked = true;
          }
        }
        if (!tracked) {
          ModuleModel localLiv = ModuleModel(name);
          localLiv.tracked = false;
          syncData[key]!.add(localLiv);
        }
      } //dir loop
    } // key loop
    return syncData;
  }

  DownloadFiles getModDownloads(syncData) {
    DownloadFiles dlFiles = DownloadFiles();
    for (String key in syncData.keys) {
      for (ModuleModel mod in syncData[key]) {
        if (!mod.tracked || mod.synced || !mod.include) {
          continue;
        }
        dlFiles.files.addAll(mod.files);
      }
    }
    return dlFiles;
  }

  void deleteLocalMod(String modName, String modType, String root) {
    assert(modName != '');
    assert(modType != '');
    assert(root != '');

    String modPath = ctx.join(root, 'mods', modType, modName);
    Directory modDirectory = Directory(modPath);
    if (modDirectory.existsSync()) {
      modDirectory.deleteSync(recursive: true);
    }
  }

  void deleteUnSyncedMods(Map<String, List<ModuleModel>> data, String root) {
    for (String key in data.keys) {
      for (ModuleModel mod in data[key]!) {
        if (!mod.tracked || mod.synced || !mod.include) {
          continue;
        }
        deleteLocalMod(mod.name, key, root);
      }
    }
  }

/*
  //testing ...
  Future<List<dynamic>> loadJsonData(String path) async {
    List data = [];
    var jsonText = await rootBundle.loadString(path);
    data = json.decode(jsonText);
    return data;
  }

  void addKeys() async {
    List privateKeyData;
    try {
      privateKeyData = await loadJsonData('assets/keys/private.json');
    } catch (e) {
      privateKeyData = [];
    }
    List publicKeyData = await loadJsonData('assets/keys/public.json');

    if (privateKeyData.isNotEmpty) {
      privKey.key =
          KeyPair(privateKey: privateKeyData.first['key'], publicKey: null);
      privKey.signer =
          privKey.key!.createSigner(algorithms.signing.hmac.sha256);
    }

    for (var pubkey in publicKeyData) {
      pubKeys.keys.add(KeyPair(publicKey: pubkey['key'], privateKey: null));
    }
  }

  Future<UploadFiles> generateUploadSignatures(UploadFiles files) async {
    if (privKey.key == null) {
      throw Exception; //TODO
    }
    Directory tempDir = Directory.systemTemp;

    try {
      for (UploadFile file in files.files) {
        Uint8List fileBytes = await file.file.readAsBytes();
        Uint8List digest = digester.process(fileBytes);
      }
    } catch (e) {
      //TODO
    }
    return files;
  }
  */
}





  //testing...
  /*
  Future<List<ModuleModel>> getMods(modType) async {
    String rootPath = await localFolder();
    if (rootPath == '') {
      throw Exception('DCS Saved Games folder is not set.');
    }

    String path = ctx.join(rootPath, 'mods', modType);
    Directory dir = Directory(path);

    List<ModuleModel> mods = [];

    await for (var folder in dir.list()) {
      String name = folder.path.split('\\').last;
      ModuleModel mod = ModuleModel(name);
      mod.setFiles(await getFilesRecursive(folder.path));
      mods.add(mod);
    }
    return mods;
  }
  */

