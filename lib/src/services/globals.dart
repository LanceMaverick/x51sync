class GlobalData {
  //static const String version = 'v0.6.0 BETA';
  //static const String versionNum = '0.6.0';
  static const String version = 'v0.8.0';
  static const String versionNum = '0.8.8';
  static const num defaultWidth = 1920;
  static const num defaultHeight = 1080;
}
