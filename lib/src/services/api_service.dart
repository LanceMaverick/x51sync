import 'dart:async';
import 'dart:convert';
import 'dart:io';

import 'package:dio/dio.dart';
import 'package:dio_smart_retry/dio_smart_retry.dart';
import 'package:f_logs/f_logs.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:http/http.dart' as http;

import 'package:x51sync/models/files.dart';
import 'package:x51sync/src/services/api_state.dart';
import 'package:x51sync/src/services/globals.dart';
import 'package:f_logs/f_logs.dart';

class ApiService {
  final String baseUrl = 'hq.x51squadron.com/fileapi';
  final String root = 'x51Sync/dcs';
  String loginToken = '';
  String username = '';
  final String s3Url = 'https://fra1.digitaloceanspaces.com/x51sqn/';
  var storage = const FlutterSecureStorage();
  final dio = Dio();

  StreamController<ApiState> uploadStreamController =
      StreamController.broadcast();
  StreamController<ApiState> downloadStreamController = StreamController();

  StreamController<Exception> errorStreamController = StreamController();

  ApiService() {
    dio.interceptors.add(RetryInterceptor(
      dio: dio,
      logPrint: logError,
      retries: 10,
    ));
  }

  void logError(String string) {
    FLog.error(text: string);
  }

  Map<String, String> _buildAuthHeader(String uname, String pwd) {
    String authString = 'Basic ${base64Encode(utf8.encode('$uname:$pwd'))}';

    Map<String, String> authHeaders = {
      'content-type': 'application/json',
      'accept': 'application/json',
      'authorization': authString
    };

    return authHeaders;
  }

  Future<Map<String, String>> _buildTokenHeader() async {
    String? token = await storage.read(key: 'token');

    if (token == null) {
      throw Exception('null token');
    }
    Map<String, String> headers = {
      'Content-Type': 'application/json',
      'x-access-tokens': token
    };

    return headers;
  }

  Uri _getUrl(String endpoint) {
    return Uri.parse('https://$baseUrl/$endpoint');
  }

  Future<String> doLogin(uname, pwd) async {
    //TODO more exceptions

    var params = {'ver': GlobalData.versionNum};

    Uri uri = Uri.https('hq.x51squadron.com', 'fileapi/login', params);
    FLog.info(text: "Login endpoint: $uri");
    http.Response response =
        await http.post(uri, headers: _buildAuthHeader(uname, pwd));
    if (response.statusCode == 200) {
      String token = json.decode(response.body)['token'];
      return token;
    } else {
      throw Exception(response.body);
    }
  }

  Future<String> validateToken() async {
    //validate token with server

    var headers = await _buildTokenHeader();

    http.Response response =
        await http.get(_getUrl('validate-token'), headers: headers);
    Map<String, dynamic> data =
        jsonDecode(response.body) as Map<String, dynamic>;
    checkStatus(response);
    FLog.info(text: "Token validation returned: ${data['message']}");
    return data['message'];
  }

  Future<List<JsonFile>> getFileList(path) async {
    // await validateToken();

    var headers = await _buildTokenHeader();

    String prefix = root + path;
    var params = {'prefix': prefix};

    Uri uri = Uri.https(
        'hq.x51squadron.com', 'fileapi/get-detailed-file-list', params);
    FLog.info(text: "Sending request to $uri with parameter prefix: $prefix");
    http.Response response = await http.get(uri, headers: headers);
    checkStatus(response);
    Map<String, dynamic> data =
        jsonDecode(response.body) as Map<String, dynamic>;

    List<JsonFile> files = [];
    for (var el in data['files']) {
      files.add(JsonFile(el));
    }
    return files;
  }

  Future<List<JsonFile>> getFiles(String folder) async {
    String path = '/$folder';
    List<JsonFile> modFiles = await getFileList(path);

    return modFiles;
  }

  Future<void> downloadFile(FileModel file) async {
    String downloadUrl = s3Url + root + file.rpath;
    FLog.info(text: "Downloading $downloadUrl");
    Response response = await dio.get(
      downloadUrl,
      //onReceiveProgress: showDownloadProgress,
      options: Options(
          responseType: ResponseType.bytes,
          followRedirects: false,
          validateStatus: (status) {
            return status! < 500;
          }),
    );
    checkStatus(response);
    FLog.info(text: "Saving ${file.file}");
    await file.file.create(recursive: true);
    var raf = file.file.openSync(mode: FileMode.write);
    raf.writeFromSync(response.data);
    await raf.close();
  }

  Future<List<String>> readRemoteFile(FileModel file) async {
    String downloadUrl = s3Url + root + file.rpath;
    FLog.info(text: "Reading remote file $downloadUrl");
    Response response = await dio.get(
      downloadUrl,
      //onReceiveProgress: showDownloadProgress,
      options: Options(
          responseType: ResponseType.bytes,
          followRedirects: false,
          validateStatus: (status) {
            return status! < 500;
          }),
    );
    checkStatus(response);
    String fileData = String.fromCharCodes(response.data);
    LineSplitter ls = const LineSplitter();

    List<String> lines = ls.convert(fileData);
    return lines;
  }

  Future<List<String>> getDCSModNames(String modType) async {
    //await validateToken();
    var headers = await _buildTokenHeader();

    var params = {'mod-type': modType};

    Uri uri = Uri.https('hq.x51squadron.com', 'fileapi/dcs/get-mods', params);
    FLog.info(text: "Retrieving mod names for category '$modType' from $uri");
    http.Response response = await http.get(uri, headers: headers);
    checkStatus(response);
    Map<String, dynamic> data =
        jsonDecode(response.body) as Map<String, dynamic>;

    List<String> modTypes = [];
    for (String el in data['files']) {
      modTypes.add(el);
    }
    //errorStreamController.sink.add(ApiAuthExpired('Session Expired')); //TODO: testing
    return modTypes;
  }

  Future<List<String>> getDCSLiveryNames(String plane) async {
    //await validateToken();

    var headers = await _buildTokenHeader();

    var params = {'aircraft': plane};

    Uri uri =
        Uri.https('hq.x51squadron.com', 'fileapi/dcs/get-liveries', params);
    FLog.info(text: "Retrieving livery names for aircraft '$plane' from $uri");
    http.Response response = await http.get(uri, headers: headers);
    checkStatus(response);
    Map<String, dynamic> data =
        jsonDecode(response.body) as Map<String, dynamic>;

    List<String> liveries = [];
    for (String el in data['files']) {
      liveries.add(el);
    }
    return liveries;
  }

  Future<List<String>> getDCSLiveryAircraft() async {
    //await validateToken();

    var headers = await _buildTokenHeader();

    //var params = {'mod-type': modType};

    Uri uri =
        Uri.https('hq.x51squadron.com', 'fileapi/dcs/get-livery-aircraft');
    FLog.info(text: "Retrieving aircraft with tracked liveries from $uri");
    http.Response response = await http.get(uri, headers: headers);
    checkStatus(response);

    Map<String, dynamic> data =
        jsonDecode(response.body) as Map<String, dynamic>;

    List<String> planes = [];
    for (String el in data['files']) {
      planes.add(el);
    }
    return planes;
  }

  Future<List<JsonFile>> getDCSMod(String modType, String mod) async {
    //await validateToken();

    var headers = await _buildTokenHeader();

    var params = {'mod-type': modType, 'mod': mod};

    Uri uri = Uri.https('hq.x51squadron.com', 'fileapi/dcs/get-mod', params);
    FLog.info(text: "Retrieving $modType/$mod from $uri");
    http.Response response = await http.get(uri, headers: headers);

    checkStatus(response);

    Map<String, dynamic> data =
        jsonDecode(response.body) as Map<String, dynamic>;
    if (data['files'] == null) {
      throw Exception('possible repo corruption');
    }
    List<JsonFile> files = [];
    for (var el in data['files']) {
      files.add(JsonFile(el));
    }
    return files;
  }

  Future<List<JsonFile>> getDCSLivery(String aircraft, String livery) async {
    //await validateToken();

    var headers = await _buildTokenHeader();

    var params = {'aircraft': aircraft, 'livery': livery};

    Uri uri = Uri.https('hq.x51squadron.com', 'fileapi/dcs/get-livery', params);
    FLog.info(text: "Retrieving $aircraft/$livery from $uri");
    http.Response response = await http.get(uri, headers: headers);
    checkStatus(response);
    Map<String, dynamic> data =
        jsonDecode(response.body) as Map<String, dynamic>;
    if (data['files'] == null) {
      throw Exception('possible repo corruption');
    }
    List<JsonFile> files = [];
    for (var el in data['files']) {
      files.add(JsonFile(el));
    }
    return files;
  }

  void deleteDCSMod(String modType, String mod) async {
    await validateToken();
    var headers = await _buildTokenHeader();
    var params = {'mod-type': modType, 'mod': mod};

    Response response = await dio.get(
        'https://hq.x51squadron.com/fileapi/dcs/delete-mod',
        queryParameters: params,
        options: Options(headers: headers));
    checkStatus(response);
  }

  Future<dynamic> getPresignedUploadUrl(UploadFile file, headers) async {
    String uploadUrl = file.remotePath!;

    Map<String, String> params = {'object': uploadUrl};
    Response response = await dio.get(
        'https://hq.x51squadron.com/fileapi/get-upload-token',
        queryParameters: params,
        options: Options(headers: headers));
    checkStatus(response);
    return response.data;
  }

  void uploadFiles(UploadFiles files, context) async {
    //debugger();
    FLog.info(text: "Uploading files with context: $context");
    Map<String, String> reqHeaders = await _buildTokenHeader();
    int totFiles = files.files.length;
    int filesRemaining = totFiles;
    int totBytes = await files.size();

    ApiTransferState state = ApiTransferState(
      context: context,
      totFiles: totFiles,
      filesRemaining: filesRemaining,
      totBytes: totBytes,
    );
    uploadStreamController.sink.add(state);

    for (UploadFile ufile in files.files) {
      File file = ufile.file;
      dynamic data = await getPresignedUploadUrl(ufile, reqHeaders);

      var len = await file.length();
      var fileData = file.openRead();

      Map<String, dynamic> headers = <String, dynamic>{
        'x-amz-acl': 'public-read',
        'content-type': 'application/octet-stream',
        "Content-Length": len
      };

      String url = data['url'];

      state = state.copyWith(
          currFile: ufile.localPath,
          currFileTotBytes: len,
          currFileSentBytes: 0);
      uploadStreamController.sink.add(state);

      Response resp = await dio.put(url,
          options: Options(headers: headers),
          data: fileData, onSendProgress: (int sent, int total) {
        state = state.copyWith(currFileSentBytes: sent);
        uploadStreamController.sink.add(state);
      });
      checkStatus(resp);
      filesRemaining -= 1;
      int newSentBytes = state.sentBytes + len;
      state = state.copyWith(
          filesRemaining: filesRemaining, sentBytes: newSentBytes);
      uploadStreamController.sink.add(state);
    }
    uploadStreamController.sink.add(ApiTransferDone(context, 0));
  }

  /// Split [files] into roughly byte-equal lists of number [nTracks].
  List<DownloadFiles> splitDownloadFiles(DownloadFiles files, int nTracks) {
    int totBytes = files.size();
    double bytesPerTrack = totBytes / nTracks;

    List<DownloadFiles> tracks = [];
    List<FileModel> dFiles = files.files;
    // Loop over all files adding the size of each onto the byteCounter.
    // If byteCounter exceeds desired amount for track, create sublist of files
    // up to that index and add it as a track. If files remain at the end of the
    // loop, add them all as the final track, sublisting from currentStartIndex.
    // If files remain after desired number of tracks are created, add them to
    // the final track.
    int byteCounter = 0;
    int currentStartIndex = 0;
    for (int i = 0; i < dFiles.length; i++) {
      // Exit conditions:
      if (tracks.length == nTracks) {
        // 1: Files remain after desired number of tracks created: Add all
        //   remaining files to the last track and break
        tracks.last.files.addAll(dFiles.sublist(i));
        break;
      }
      if (i == dFiles.length - 1) {
        // 2: Reached the end of the file loop: add all remaining files since
        //    last sublisting as the final track
        DownloadFiles track = DownloadFiles();
        track.files = dFiles.sublist(currentStartIndex);
        tracks.add(track);
      }

      byteCounter += dFiles[i].repoSize ?? 0;

      if (byteCounter > bytesPerTrack) {
        DownloadFiles track = DownloadFiles();
        track.files = dFiles.sublist(currentStartIndex, i);
        tracks.add(track);
        // reset byteCounter and currentStartIndex
        byteCounter = 0;
        currentStartIndex = i;
      }
    }
    return tracks;
  }

  void downloadMultiFiles(
      DownloadFiles files, String context, int nTracks) async {
    FLog.info(
        text:
            "Downloading multi-files in $nTracks async tracks with context: $context");
    List<DownloadFiles> tracks = splitDownloadFiles(files, nTracks);
    for (int i = 0; i < tracks.length; i++) {
      downloadFiles(tracks[i], context, track: i);
    }
  }

  Future<void> downloadFiles(DownloadFiles files, String context,
      {int track = 0}) async {
    //debugger();
    FLog.info(text: "Downloading files with context: $context");
    int totFiles = files.nFiles();
    int filesRemaining = totFiles;
    int totBytes = files.size();

    ApiTransferState state = ApiTransferState(
        context: context,
        totFiles: totFiles,
        filesRemaining: filesRemaining,
        totBytes: totBytes,
        track: track);
    downloadStreamController.sink.add(state);

    for (FileModel file in files.files) {
      String downloadUrl = s3Url + root + file.rpath;

      var fileBytes = file.repoSize!;

      state = state.copyWith(
          track: track,
          currFile: file.path,
          currFileTotBytes: fileBytes,
          currFileSentBytes: 0);
      downloadStreamController.sink.add(state);

      //temporary retry fix until Dio intercepters can handle it...
      int retries = 10;
      while (retries > 0) {
        try {
          Response resp = await dio.download(downloadUrl, file.file.path,
              onReceiveProgress: (int rec, int total) {
            state = state.copyWith(currFileSentBytes: rec, track: track);
            downloadStreamController.sink.add(state);
          });
          checkStatus(resp);
          break;
        } catch (e) {
          if (retries == 1) {
            rethrow;
          }
          logError(
              "Request failed for ${file.path} with $e. Retrying (${10 - retries}/10)...");
          retries -= 1;
          sleep(const Duration(seconds: 3));
        }
      }

      filesRemaining -= 1;
      int newSentBytes = state.sentBytes + fileBytes;

      state = state.copyWith(
          track: track,
          filesRemaining: filesRemaining,
          sentBytes: newSentBytes);
      downloadStreamController.sink.add(state);
    }
    downloadStreamController.sink.add(ApiTransferDone(context, track));
  }

  Future<void> deleteDCSLivery(String aircraft, String livery) async {
    var headers = await _buildTokenHeader();
    var params = {'aircraft': aircraft, 'livery': livery};

    Response response = await dio.get(
        'https://hq.x51squadron.com/fileapi/dcs/delete-livery',
        queryParameters: params,
        options: Options(headers: headers));
    checkStatus(response);
  }

  Future<void> deleteDCSLiveryAircraft(String aircraft) async {
    var headers = await _buildTokenHeader();
    var params = {'aircraft': aircraft};

    Response response = await dio.get(
        'https://hq.x51squadron.com/fileapi/dcs/delete-livery-aircraft',
        queryParameters: params,
        options: Options(headers: headers));
    checkStatus(response);
  }

  void checkStatus(response) {
    //
    //Stream the errors for the session bloc to listen to.
    Exception? e;
    int code = response.statusCode;
    if (code == 401) {
      e = ApiAuthExpired('Session Expired');
    } else if (code == 503) {
      e = ApiTimeout('No response from the server. Please try again later.');
    } else if (code != 200) {
      e = ApiGenericError(
          'Something went wrong: Server error ${response.statusCode}');
    }
    if (e != null) {
      FLog.error(text: "Failed http status code check: $e");
      errorStreamController.sink.add(e);
      throw e;
    }
  }
}
