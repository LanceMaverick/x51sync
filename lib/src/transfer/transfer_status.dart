import 'package:x51sync/models/files.dart';
import 'package:x51sync/src/services/api_state.dart';

abstract class TransferStatus {
  const TransferStatus();
}

class InitialTransferStatus extends TransferStatus {
  const InitialTransferStatus();
}

class GettingModList extends TransferStatus {}

class ProcessingFiles extends TransferStatus {}

class ProcessingComplete extends TransferStatus {}

class SubmissionRequested extends TransferStatus {}

class ProcessingFailed extends TransferStatus {
  final Object exception;

  ProcessingFailed(this.exception);
}

class SingleFileTransferStatus {
  String fileBytesSent;
  String fileBytesTot;
  double filePercent;
  String file;
  SingleFileTransferStatus(
      this.fileBytesSent, this.fileBytesTot, this.filePercent, this.file);
}

class TransferringSingleFile {
  int totalFiles;
  int filesSent;
  late String totalBytes;
  late String sentBytes;
  late String fileBytesSent;
  late String fileBytesTot;
  int totalBytesInt;
  int sentBytesInt;
  int fileBytesSentInt;
  int fileBytesTotInt;
  double totPercent;
  double filePercent;
  String file;
  TransferringSingleFile(
      this.totalFiles,
      this.filesSent,
      this.totalBytesInt,
      this.sentBytesInt,
      this.fileBytesSentInt,
      this.fileBytesTotInt,
      this.totPercent,
      this.filePercent,
      this.file) {
    totalBytes = bytesString(totalBytesInt);
    sentBytes = bytesString(sentBytesInt);
    fileBytesSent = bytesString(fileBytesSentInt);
    fileBytesTot = bytesString(fileBytesSentInt);
  }
  TransferringSingleFile.fromApiState(ApiTransferState apiState)
      : totalFiles = apiState.totFiles,
        filesSent = apiState.totFiles - apiState.filesRemaining,
        totalBytes = bytesString(apiState.totBytes),
        sentBytes = bytesString(apiState.sentBytes),
        fileBytesSent = bytesString(apiState.currFileSentBytes),
        fileBytesTot = bytesString(apiState.currFileTotBytes),
        totalBytesInt = apiState.totBytes,
        sentBytesInt = apiState.sentBytes,
        fileBytesSentInt = apiState.currFileSentBytes,
        fileBytesTotInt = apiState.currFileTotBytes,
        totPercent = apiState.totPercent,
        filePercent = apiState.filePercent,
        file = apiState.currFile;
}

class TransferringFiles extends TransferStatus {
  int totalFiles;
  int filesSent;
  String totalBytes;
  String sentBytes;
  double totPercent;
  List<TransferringSingleFile> tracks = [];
  TransferringFiles(this.totalFiles, this.filesSent, this.totalBytes,
      this.sentBytes, this.totPercent, nTracks) {
    for (int i = 0; i < nTracks; i++) {
      tracks.add(TransferringSingleFile(0, 0, 0, 0, 0, 0, 0, 0, ''));
    }
  }
  void updateState(ApiTransferState apiState) {
    tracks[apiState.track] = TransferringSingleFile.fromApiState(apiState);
    _calculate();
  }

  void _calculate() {
    totalFiles = tracks.fold(0, (tot, item) => tot + item.totalFiles);
    filesSent = tracks.fold(0, (tot, item) => tot + item.filesSent);
    totalBytes =
        bytesString(tracks.fold(0, (tot, item) => tot + item.totalBytesInt));
    sentBytes =
        bytesString(tracks.fold(0, (tot, item) => tot + item.sentBytesInt));
    totPercent = filesSent / totalFiles;
  }
}

class TransferFileError extends TransferStatus {
  final String error;
  TransferFileError(this.error);
}

class TransferComplete extends TransferStatus {}

class TransferAuthFailed extends TransferStatus {}

class TransferNoPrivelages extends TransferStatus {}

class TransferFailed extends TransferStatus {
  final Object? exception;

  TransferFailed(this.exception);
}
