import 'package:x51sync/models/files.dart';
import 'package:x51sync/src/services/api_state.dart';

abstract class FileSyncEvent {}

class ModDataRefreshed extends FileSyncEvent {
  final dynamic syncData;
  ModDataRefreshed({this.syncData});
}

//dispatched immediately in the BlocProvider to load
// saved configuragtions/caches.
class FileSyncInit extends FileSyncEvent {}

// user uses the folder picker to change the local root folder
class ChangeRootFolder extends FileSyncEvent {
  final String? root;

  ChangeRootFolder({this.root});
}

class ChangeColorMode extends FileSyncEvent {
  final bool? colorblindMode;
  ChangeColorMode({this.colorblindMode});
}

//dispatched from UI. Request refresh of mod data.
class FileSyncRefreshModData extends FileSyncEvent {}

//Intermediate events for data loading. Currently unused. --->
class RemoteDataRequested extends FileSyncEvent {}

class RemoteDataLoading extends FileSyncEvent {}

class LocalDataRequested extends FileSyncEvent {}

class LocalDataLoading extends FileSyncEvent {}
//<--- Intermediate events for data loading. Currently unused.

class FileSyncModSyncRequested extends FileSyncEvent {}

class FileSyncRefreshLivData extends FileSyncEvent {}

class FileSyncLivSyncRequested extends FileSyncEvent {}

class FileSyncChecking extends FileSyncEvent {}

class FileSyncChecked extends FileSyncEvent {}

class FileSyncApiEvent extends FileSyncEvent {
  final ApiTransferState event;
  FileSyncApiEvent(this.event);
}

class FileSyncCompleted extends FileSyncEvent {}

class ModFileSyncCompleted extends FileSyncEvent {
  final int track;
  ModFileSyncCompleted(this.track);
}

class LivFileSyncCompleted extends FileSyncEvent {
  final int track;
  LivFileSyncCompleted(this.track);
}

class ModFileSyncFailed extends FileSyncEvent {
  Object? exception;
  ModFileSyncFailed(this.exception);
}

class LivFileSyncFailed extends FileSyncEvent {
  Object? exception;
  LivFileSyncFailed(this.exception);
}

class ModFileSyncCheckbox extends FileSyncEvent {
  final ModuleModel mod;
  final bool include;
  ModFileSyncCheckbox(this.mod, this.include);
}

class FileAuthError extends FileSyncEvent {}
