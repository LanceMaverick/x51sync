import 'dart:async';

import 'package:f_logs/f_logs.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:shared_preferences/shared_preferences.dart';

import 'package:x51sync/models/files.dart';
import 'package:x51sync/src/file_sync/bloc/files_event.dart';
import 'package:x51sync/src/file_sync/bloc/files_state.dart';
import 'package:x51sync/src/file_sync/data_state.dart';
import 'package:x51sync/src/transfer/transfer_status.dart';
import 'package:x51sync/src/services/api_service.dart';
import 'package:x51sync/src/services/api_state.dart';
import 'package:x51sync/src/services/file_service.dart';
import 'package:x51sync/src/services/sync_service.dart';

class FileSyncBloc extends Bloc<FileSyncEvent, FileSyncState> {
  final FileService fileSvc;
  final ApiService apiSvc;
  final SyncService syncSvc;
  // ignore: unused_field
  StreamSubscription<ApiState>? _downloadSubscription;

  FileSyncBloc(
      {required this.fileSvc, required this.apiSvc, required this.syncSvc})
      : super(FileSyncState()) {
    _downloadSubscription =
        apiSvc.downloadStreamController.stream.listen((apiState) {
      if (apiState is ApiTransferState) {
        add(FileSyncApiEvent(apiState));
      } else if (apiState is ApiTransferDone) {
        if (apiState.context == 'mod') {
          add(ModFileSyncCompleted(apiState.track));
        } else if (apiState.context == 'liv') {
          add(LivFileSyncCompleted(apiState.track));
        }
      } else if (apiState is ApiTransferFailed) {
        String msg =
            "Error downloading file ${apiState.currFile}: ${apiState.e.toString()}";
        Exception e = Exception(msg);
        if (apiState.context == 'mod') {
          add(ModFileSyncFailed(e));
        } else if (apiState.context == 'liv') {
          add(LivFileSyncFailed(e));
        }
      }
    });

    //mod handlers
    on<FileSyncInit>(_onFileSyncInit);
    on<FileSyncRefreshModData>(_onRefreshModData);
    on<ModFileSyncCheckbox>(_onModFileSyncCheckbox);

    on<FileSyncModSyncRequested>(_onModSync);
    on<ModFileSyncFailed>(_onModFileSyncFailed);

    //livery handlers
    on<FileSyncRefreshLivData>(_onRefreshLivData);
    on<FileSyncLivSyncRequested>(_onLivSync);
    on<LivFileSyncCompleted>(onLivFileSyncCompleted);
    on<LivFileSyncFailed>(_onLivFileSyncFailed);

    //api handlers
    on<FileSyncApiEvent>(onFileSyncApiEvent);
    on<ModFileSyncCompleted>(onModFileSyncCompleted);

    //global handlers
    on<ChangeRootFolder>(_onChangeRootFolder);
    on<ChangeColorMode>(_onChangeColorMode);
  }

  Future<Map<String, List<ModuleModel>>> _getModSyncData(state) async {
    FLog.info(text: "Generating mod data");
    Map<String, List<ModuleModel>> syncData = {
      'aircraft': [],
      'tech': [],
      'services': [],
    };
    await apiSvc.validateToken();
    for (String modType in fileSvc.modDirs) {
      List<String> mods = await apiSvc.getDCSModNames(modType);
      for (String modName in mods) {
        //TODO.. find a better way to handle data structure corruption

        List<JsonFile> modFiles = await apiSvc.getDCSMod(modType, modName);
        ModuleModel mod = ModuleModel(modName);
        mod.tracked = true;
        mod.parent = modType;

        mod = await syncSvc.modFileData(mod, modFiles, state.root);

        syncData[modType]!.add(mod);
      }
    }

    syncData = await fileSvc.addUntrackedMods(state.root ?? '', syncData);

    return syncData;
  }

  Future<Map<String, List<ModuleModel>>> _getLivSyncData(state) async {
    FLog.info(text: "Generating livery data");
    Map<String, List<ModuleModel>> syncData = {};
    await apiSvc.validateToken();
    //get list of aircraft that have tracked liveries
    List<String> planes = await apiSvc.getDCSLiveryAircraft();

    for (String planeName in planes) {
      //get livery names for each plane
      List<String> liveries = await apiSvc.getDCSLiveryNames(planeName);
      for (String liveryName in liveries) {
        List<JsonFile> liveryFiles =
            await apiSvc.getDCSLivery(planeName, liveryName);
        ModuleModel livery = ModuleModel(liveryName);
        livery.tracked = true;
        livery.parent = planeName;
        livery = await syncSvc.modFileData(livery, liveryFiles, state.root);
        if (!syncData.containsKey(planeName)) {
          syncData[planeName] = [];
        }
        syncData[planeName]!.add(livery);
      }
    }

    syncData = await fileSvc.addUntrackedLiveries(state.root ?? '', syncData);

    return syncData;
  }

  void _onFileSyncInit(FileSyncInit event, Emitter<FileSyncState> emit) async {
    //on initialize, grab a root directory if it's saved
    FLog.info(text: "Initializing FileSync state");

    SharedPreferences prefs = await SharedPreferences.getInstance();
    String root = prefs.getString('root') ?? '';
    bool canUpload = await apiSvc.validateToken() == 'curator';
    bool colorblindMode = prefs.getBool('colorblindMode') ?? false;

    emit(state.copyWith(
        root: root,
        modDataStatus: const InitialDataStatus(),
        canUpload: canUpload,
        colorblindMode: colorblindMode));
  }

  void _onRefreshModData(
      FileSyncRefreshModData event, Emitter<FileSyncState> emit) async {
    // mod data refresh requested
    FLog.info(text: "Refreshing mod data");
    emit(state.copyWith(modDataStatus: RemoteDataRefreshing()));
    Map<String, List<ModuleModel>> syncData = await _getModSyncData(state);
    emit(state.copyWith(modSyncData: syncData, modDataStatus: DataRefreshed()));
  }

  void _onModFileSyncCheckbox(
      ModFileSyncCheckbox event, Emitter<FileSyncState> emit) async {
    FLog.info(
        text:
            "Changing active sync state for ${event.mod.name} to ${event.include}");
    String parent = event.mod.parent;
    Map<String, List<ModuleModel>> mods = state.modSyncData!;
    for (ModuleModel mod in mods[parent]!) {
      if (mod.name == event.mod.name) {
        mod.include = event.include;
      }
    }
    emit(state.copyWith(modSyncData: mods));
  }

  void _onModSync(
      FileSyncModSyncRequested event, Emitter<FileSyncState> emit) async {
    //local mod sync with remote requested
    FLog.info(text: "Mod data sync requested");
    DownloadFiles downloads = fileSvc.getModDownloads(state.modSyncData);

    fileSvc.deleteUnSyncedMods(state.modSyncData!, state.root!);
    int nTracks = 3;
    List<bool> trackStatus = [];
    for (int i = 0; i < nTracks; i++) {
      //TODO convert to map
      trackStatus.add(false);
    }
    emit(state.copyWith(
        trackStatus: trackStatus,
        modDataStatus: DataSyncing(),
        syncStatus: TransferringFiles(0, 0, '', '', 0, nTracks)));
    try {
      apiSvc.downloadMultiFiles(downloads, 'mod', nTracks = nTracks);
    } catch (e) {
      add(ModFileSyncFailed(e));
    }
  }

  void _onRefreshLivData(
      FileSyncRefreshLivData event, Emitter<FileSyncState> emit) async {
    FLog.info(text: "Refreshing livery data");
    // mod data refresh requested
    emit(state.copyWith(livDataStatus: RemoteDataRefreshing()));
    Map<String, List<ModuleModel>> syncData = await _getLivSyncData(state);
    emit(state.copyWith(livSyncData: syncData, livDataStatus: DataRefreshed()));
  }

  void _onLivSync(
      FileSyncLivSyncRequested event, Emitter<FileSyncState> emit) async {
    //local mod sync with remote requested
    FLog.info(text: "Livery data sync requested");
    DownloadFiles downloads = fileSvc.getModDownloads(state.livSyncData);
    int nTracks = 3;
    List<bool> trackStatus = [];
    for (int i = 0; i < nTracks; i++) {
      //TODO convert to map
      trackStatus.add(false);
    }
    emit(state.copyWith(
        trackStatus: trackStatus,
        livDataStatus: DataSyncing(),
        syncStatus: TransferringFiles(0, 0, '', '', 0, nTracks)));
    apiSvc.downloadMultiFiles(downloads, 'liv', nTracks = nTracks);
  }

  void _onChangeRootFolder(
      ChangeRootFolder event, Emitter<FileSyncState> emit) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    await prefs.setString('root', event.root ?? '');
    FLog.info(
        text: "Root directory changed from ${state.root} to ${event.root}");
    emit(state.copyWith(root: event.root));
  }

  void _onChangeColorMode(
      ChangeColorMode event, Emitter<FileSyncState> emit) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    await prefs.setBool('colorblindMode', event.colorblindMode ?? false);
    FLog.info(
        text:
            "Interface colorblind mode changed from ${state.colorblindMode} to ${event.colorblindMode}");
    emit(state.copyWith(colorblindMode: event.colorblindMode));
  }

  void onFileSyncApiEvent(FileSyncApiEvent event, Emitter<FileSyncState> emit) {
    TransferStatus newSyncState = state.syncStatus;
    if (newSyncState is TransferringFiles) {
      newSyncState.updateState(event.event);
    }
    emit(state.copyWith(syncStatus: newSyncState));
  }

  void onModFileSyncCompleted(
      ModFileSyncCompleted event, Emitter<FileSyncState> emit) {
    if (state.syncStatus is TransferringFiles) {
      state.trackStatus[event.track] = true;
      emit(state.copyWith());
    }
    //bool totalState =
    if (state.trackStatus.every((item) => item == true)) {
      FLog.info(text: "Mod data sync completed");

      emit(state.copyWith(
          syncStatus: TransferComplete(), modDataStatus: SyncDataDone()));
    }
  }

  void onLivFileSyncCompleted(
      LivFileSyncCompleted event, Emitter<FileSyncState> emit) {
    if (state.syncStatus is TransferringFiles) {
      state.trackStatus[event.track] = true;
      emit(state.copyWith());
    }
    //bool totalState =
    if (state.trackStatus.every((item) => item == true)) {
      FLog.info(text: "Liv data sync completed");

      emit(state.copyWith(
          syncStatus: TransferComplete(), livDataStatus: SyncDataDone()));
    }
  }

  void _onModFileSyncFailed(
      ModFileSyncFailed event, Emitter<FileSyncState> emit) {
    FLog.error(text: "Mod data sync failed with: ${event.exception}");
    emit(state.copyWith(
        syncStatus: TransferFailed(event.exception),
        modDataStatus: const InitialDataStatus()));
  }

  void _onLivFileSyncFailed(
      LivFileSyncFailed event, Emitter<FileSyncState> emit) {
    FLog.error(text: "Livery data sync failed with: ${event.exception}");
    emit(state.copyWith(
        syncStatus: TransferFailed(event.exception),
        livDataStatus: const InitialDataStatus()));
  }
}
