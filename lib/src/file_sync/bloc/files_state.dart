import 'package:x51sync/models/files.dart';
import 'package:x51sync/src/file_sync/data_state.dart';
import 'package:x51sync/src/transfer/transfer_status.dart';

class FileSyncState {
  final String? root;
  final List<JsonFile>? remoteModFiles;
  final Map<String, List<ModuleModel>>? localModsData;
  final Map<String, List<ModuleModel>>? modSyncData;
  final Map<String, List<ModuleModel>>? livSyncData;
  final DataStatus modDataStatus;
  final DataStatus livDataStatus;
  final TransferStatus syncStatus;
  final bool canUpload;
  final bool colorblindMode;
  List<bool> trackStatus = [];

  //final RemoteFiles formStatus;

  FileSyncState(
      {this.root,
      this.remoteModFiles,
      this.localModsData,
      this.modSyncData,
      this.livSyncData,
      this.modDataStatus = const InitialDataStatus(),
      this.livDataStatus = const InitialDataStatus(),
      this.syncStatus = const InitialTransferStatus(),
      this.canUpload = false,
      this.colorblindMode = false,
      this.trackStatus = const []});

  get key => null;

  FileSyncState copyWith(
      {String? root,
      bool? colorblindMode,
      List<JsonFile>? remoteModFiles,
      Map<String, List<ModuleModel>>? localModsData,
      Map<String, List<ModuleModel>>? modSyncData,
      Map<String, List<ModuleModel>>? livSyncData,
      DataStatus? modDataStatus,
      DataStatus? livDataStatus,
      TransferStatus? syncStatus,
      bool? canUpload,
      List<bool>? trackStatus}) {
    return FileSyncState(
        root: root ?? this.root,
        colorblindMode: colorblindMode ?? this.colorblindMode,
        remoteModFiles: remoteModFiles ?? this.remoteModFiles,
        localModsData: localModsData ?? this.localModsData,
        modSyncData: modSyncData ?? this.modSyncData,
        livSyncData: livSyncData ?? this.livSyncData,
        modDataStatus: modDataStatus ?? this.modDataStatus,
        livDataStatus: livDataStatus ?? this.livDataStatus,
        syncStatus: syncStatus ?? this.syncStatus,
        canUpload: canUpload ?? this.canUpload,
        trackStatus: trackStatus ?? this.trackStatus);
  }
}
