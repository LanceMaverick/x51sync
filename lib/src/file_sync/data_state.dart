abstract class DataStatus {
  const DataStatus();
}

class InitialDataStatus extends DataStatus {
  const InitialDataStatus();
}

class DataRefreshing extends DataStatus {}

class RemoteDataRefreshing extends DataRefreshing {}

class SyncDataRefreshing extends DataRefreshing {}

class LocalDataRefreshing extends DataRefreshing {}

class DataRefreshed extends DataStatus {}

class DataSyncing extends DataStatus {
  DataSyncing();
}

class SyncAuthFailed extends DataStatus {}

class SyncDataDone extends DataStatus {}

class DataRefreshFailed extends DataStatus {
  final Object exception;

  DataRefreshFailed(this.exception);
}
